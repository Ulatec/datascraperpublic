package DataScraper.Model;

import com.opencsv.bean.CsvBindByName;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class VINEntry {

    @NotNull
    @CsvBindByName(column="VIN")
    private String vin;
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private Date inspection;
    private Date dateFound;
    private String station;
    private boolean found;
    private String state;
    private String price;
    private Date regDate;
    private boolean nonUS;
    private int vinNumber;
    private boolean inNmvtis;
    private Date NMVTISDate;
    private boolean carfaxFound;
    private Date carfaxFoundDate;
    private String trim;
    private String city;
    private boolean accident;
    private boolean autoCheckFound;
    private Date autoCheckDate;
    private String country;
    public boolean isAccident() {
        return accident;
    }

    public void setAccident(boolean accident) {
        this.accident = accident;
    }

    public VINEntry(){
        found = false;
    }
    public VINEntry(String vin){

        this.vin = vin;
        found = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Date getInspection() {
        return inspection;
    }

    public void setInspection(Date inspection) {
        this.inspection = inspection;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public boolean isFound() {
        return found;
    }

    public void setFound(boolean found) {
        this.found = found;
    }

    public Date getDateFound() {
        return dateFound;
    }

    public void setDateFound(Date dateFound) {
        this.dateFound = dateFound;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public boolean isNonUS() {
        return nonUS;
    }

    public void setNonUS(boolean nonUS) {
        this.nonUS = nonUS;
    }


    public int getVinNumber() {
        return vinNumber;
    }

    public void setVinNumber(int vinNumber) {
        this.vinNumber = vinNumber;
    }


    public boolean isInNMVTIS() {
        return inNmvtis;
    }

    public void setInNMVTIS(boolean inNMVTIS) {
        this.inNmvtis = inNMVTIS;
    }

    public Date getNMVTISDate() {
        return NMVTISDate;
    }

    public void setNMVTISDate(Date NMVTISDate) {
        this.NMVTISDate = NMVTISDate;
    }

    public boolean isInNmvtis() {
        return inNmvtis;
    }

    public void setInNmvtis(boolean inNmvtis) {
        this.inNmvtis = inNmvtis;
    }

    public boolean isCarfaxFound() {
        return carfaxFound;
    }

    public void setCarfaxFound(boolean carfaxFound) {
        this.carfaxFound = carfaxFound;
    }

    public String getTrim() {
        return trim;
    }

    public void setTrim(String trim) {
        this.trim = trim;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isAutoCheckFound() {
        return autoCheckFound;
    }

    public void setAutoCheckFound(boolean autoCheckFound) {
        this.autoCheckFound = autoCheckFound;
    }

    public Date getAutoCheckDate() {
        return autoCheckDate;
    }

    public void setAutoCheckDate(Date autoCheckDate) {
        this.autoCheckDate = autoCheckDate;
    }

    public Date getCarfaxFoundDate() {
        return carfaxFoundDate;
    }

    public void setCarfaxFoundDate(Date carfaxFoundDate) {
        this.carfaxFoundDate = carfaxFoundDate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
