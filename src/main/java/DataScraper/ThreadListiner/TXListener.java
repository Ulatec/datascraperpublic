package DataScraper.ThreadListiner;
//TODO: Consolidate all Listener classes into one class, and pass required strings during initialization.
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;

import org.apache.commons.net.ftp.FTPClient;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.PushCommand;

import org.eclipse.jgit.api.errors.GitAPIException;


import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.transport.PushResult;
import org.eclipse.jgit.transport.RemoteConfig;

import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.io.*;

import java.sql.Connection;
import java.sql.DriverManager;

import java.sql.Statement;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@PropertySource("application.properties")
public class TXListener extends Thread{
    @Autowired
    private Environment env;
    private final String ACCOUNT_SID = "ACd36e4fae72d52ebcd8bc5593952fb6d2";
    private final String AUTH_TOKEN = "f28cac58baa3e7f177cd7178aa0f45d2";
    private boolean kill;
    private int numOfThreads;
    private int killedThreads;

    public TXListener(int numOfThreads){
        kill = false;
        this.numOfThreads = numOfThreads;
        killedThreads = 0;
        System.out.println("TXListener Created");
    }

    public void run(){
        System.out.println("TX LISTENER RUN");
        while(!kill){
            Thread.currentThread().interrupt();
            return;
        }
        System.out.println("Goodbye.");
    }

    private void checkIfKill(){
        if(killedThreads >= numOfThreads){
            //Once all threads are dead/finished export latest results from DB to local machine, and upload results to GitHub Repo.
            callDBExport();
            kill = true;
            System.out.println("Goodbye.");
            Thread.currentThread().interrupt();
        }else {
            System.out.println("no kill");
        }
    }

    public void threadFinish(int threadNum){
        killedThreads = killedThreads + 1;
        System.out.println(threadNum + " has finished. " + killedThreads);

        checkIfKill();
    }

    public void reportCounter(int counter, int threadNum){
        System.out.println(counter + " Have been run. [listener] [" + threadNum + "]");
    }

    public void callDBExport() {
        Session session;
        JSch jsch = new JSch();
        Channel channel = null;
        Git git = null;
        try {
            git = Git.open(new File("./M3Vins/"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        File file = new File("./M3Vins/");
        PullResult pullCommand = null;
        try {
            assert git != null;
            pullCommand = git.pull().setRebase(true).setRemote("origin").setRemoteBranchName("master").setCredentialsProvider(new UsernamePasswordCredentialsProvider("ulatec@gmail.com", "")).call();
        } catch (GitAPIException e) {
            e.printStackTrace();
        }
        System.out.println(pullCommand);
        try {
            Connection conn;
            Statement stmt;
            String JDBC_DRIVER = "org.h2.Driver";
            String DB_URL = "jdbc:h2:tcp://142.11.241.249/~/../home/mark/data;CACHE_SIZE=131072";
            String USER = "sa";
            String PASS = "";
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql = "CALL CSVWRITE('VinDataTX.csv', 'SELECT VIN,INSPECTION,STATION,DATEFOUND FROM VINENTRY WHERE STATE=''TX'' or INSPECTION IS NOT NULL')";
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();

            session = jsch.getSession("root", "142.11.241.249", 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword("");
            session.connect();

            channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            sftpChannel.get("/home/mark/VinDataTX.csv", "./M3Vins/");
            sftpChannel.exit();
            session.disconnect();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            assert channel != null;
            if (channel.isConnected()) {
                channel.disconnect();
            }
        }
        gitHubUpload(git);
    }

    private void gitHubUpload(Git git) {
        try {

            List<RemoteConfig> remotes = git.remoteList().call();

            Iterable<RevCommit> commits = git.log().all().call();
            for(RevCommit commit : commits){
                System.out.println(commit.getName());
                System.out.println(commit.getFullMessage());
            }

            git.add().setUpdate(true);
            git.add().addFilepattern("VinDataTX.csv").call();

            RevCommit commit = git.commit().setMessage(new Date().toString()).call();
            System.out.println(commit.getTree());
// push to remote:
            PushCommand pushCommand = git.push();
            pushCommand.setCredentialsProvider(new UsernamePasswordCredentialsProvider("ulatec@gmail.com", ""));
            pushCommand.setPushAll();
            pushCommand.setRemote("origin");
            for(RemoteConfig remote: remotes){
                System.out.println(remote.getFetchRefSpecs());
            }

            Iterable<PushResult> pushResults = pushCommand.setForce(true).call();
            for(PushResult pushResult : pushResults){
                System.out.println(pushResult.getRemoteUpdates());

            }
            //Send push notification via SMS that scrape is done.
            if(env.getProperty("scraper.notification.phone") != null) {
                if(Objects.equals(env.getProperty("scraper.notification.enabled"), "true")) {
                    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
                    Message smsMessage = Message.creator(
                            new com.twilio.type.PhoneNumber(env.getProperty("scraper.notification.phone")),
                            new com.twilio.type.PhoneNumber("+14697131789"),
                            "Texas uploaded.").create();
                    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
                    System.out.println(smsMessage.getSid());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
