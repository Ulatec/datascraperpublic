package DataScraper.Utils;

import DataScraper.Model.VINEntry;

import java.util.List;

public class ListTrimmer {

    public static List<VINEntry> random(List<VINEntry> vins, int percentToReturn){
        int finalSize = (int) (vins.size()*((double)percentToReturn * 0.01));
        for(int i = vins.size() - 1; i> finalSize; i--){
            int rand = (int)(Math.random()*i);
            vins.remove(rand);
        }
        return vins;
    }
}
