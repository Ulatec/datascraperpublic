package DataScraper.Utils;

import DataScraper.Model.VINEntry;
import DataScraper.Service.VinService;

import java.util.List;

public class Duplicates {

    public static void test(VinService vinService, VINEntry entry){

            List<VINEntry> matchedEntries = vinService.findByVinNumber(entry.getVinNumber());
            for (VINEntry testentry : matchedEntries) {
                if (!testentry.getVin().equals(entry.getVin())) {
                    if(!testentry.isCarfaxFound() && !testentry.isInNMVTIS() && !testentry.isFound()) {
                        System.out.println(testentry.getVin() + " does NOT match " + entry.getVin());
                        System.out.println("Let's delete: " + testentry.getVin() + " it was not found anywhere.");
                        System.out.println(testentry.getVin() + " : " + testentry.isInNMVTIS() + " : " + testentry.isFound() + " : " + testentry.isCarfaxFound() + " : " + testentry.getInspection());
                        vinService.delete(testentry);
                    }
                } else {
                    System.out.println(testentry.getVin() + " does match " + entry.getVin() + ". Same vin. Do not Delete.");
                }
            }
        }
    public static void testForIdenticalVin(VinService vinService, VINEntry entry){

        List<VINEntry> matchedEntries = vinService.findByVin(entry.getVin());
        for (VINEntry testEntry : matchedEntries) {
            if (testEntry.getVin().equals(entry.getVin()) && !testEntry.getId().equals(entry.getId())) {
                System.out.println(testEntry.getVin() + " does match " + entry.getVin());
                System.out.println(testEntry.getId() + " " + entry.getId());
                System.out.println("delete");
                vinService.delete(testEntry);
            } else {
                System.out.println(testEntry.getVin() + " does match " + entry.getVin());
            }
        }
    }
}
