package DataScraper.Utils;

import DataScraper.Model.VINEntry;

import java.util.ArrayList;
import java.util.List;

public class ListSplitter {

    public static ArrayList<List<VINEntry>> split(List<VINEntry> fulllist, int numOfOutputs, int start) {
        // get size of the list
        List<VINEntry> list = fulllist.subList(start,fulllist.size());
        int size = list.size();
        ArrayList<List<VINEntry>> lists = new ArrayList<>();
        int lastDivider = 0;
        for(int i = 0;i < numOfOutputs; i++){
            int newDivider = (int) Math.round(size*((i+1)/(double)numOfOutputs));
            lists.add(new ArrayList<>(list.subList(lastDivider, newDivider)));
            lastDivider = newDivider + 1;
        }
        // return an List array of lists
        return lists;
    }
}
