package DataScraper.Service;

import DataScraper.Model.VINEntry;
import DataScraper.Repository.VinRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Date;
import java.util.List;


@Service
public class VinServiceImpl implements VinService {

    @Autowired
    private VinRepository repo;

    @Override
    public List<VINEntry> findAll() {
        return (List<VINEntry>) repo.findAll();
    }
    @Override
    public Iterable<VINEntry> findAllTimeTest(Date start) {
        Iterable<VINEntry> results = repo.findAll();
        Date finish = new Date();
        long totaltime = finish.getTime() - start.getTime();
        System.out.println(totaltime + " ms");
        return results;
    }
    @Override
    public void save(VINEntry vin) {
        repo.save(vin);
    }

    @Override
    public void delete(VINEntry vin) {
        repo.delete(vin);
    }

    @Override
    public List<VINEntry> findAllUnknown(){
        return repo.findByFoundFalseAndInNmvtisFalseAndCarfaxFoundFalseAndNonUSFalse();
    }

    @Override
    public List<VINEntry> findAllKnown(){
        return repo.findByFoundTrue();
    }

    @Override
    public List<VINEntry> findByVinNumberGreaterThan(int i) {
        return null;
    }

    @Override
    public List<VINEntry> findByVin(String vin) {
        return repo.findByVin(vin);
    }
    @Override
    public List<VINEntry> findByIdRange(int i, int j){
        return repo.findByIdGreaterThanAndIdLessThan((long) i, (long) j);
    }
    @Override
    public List<VINEntry> findAllNotInNmvtis(){
        return repo.findByInNmvtisFalse();
    }
    @Override
    public List<VINEntry> findAllInNmvtis(){
        return repo.findByInNmvtisTrue();
    }
    @Override
    public List<VINEntry> findAllNotInNmvtisAndInUS(){
        return repo.findByInNmvtisFalseAndNonUSFalse();
    }

    @Override
    public List<VINEntry> findAllNotFound() {
        return repo.findByFoundFalse();
    }

    @Override
    public List<VINEntry> findAllNotInCarfax(){
        return repo.findByCarfaxFoundFalse();
    }
    @Override
    public List<VINEntry> findAllNotInCarfaxAndInUS(){
        return repo.findByCarfaxFoundFalseAndNonUSFalse();
    }

    @Override
    public List<VINEntry> findAllNotInAutoCheckAndInUS() {
        return repo.findByAutoCheckFoundFalseAndNonUSFalse();
    }

    @Override
    public List<VINEntry> findByVinNumber(int vinNumber){
        return repo.findByVinNumber(vinNumber);
    }
    @Override
    public List<VINEntry> findAllInCarfaxAndNotInNmvtis() {
        return repo.findByCarfaxFoundTrueAndInNmvtisFalseAndStateIsNull();
    }
    @Override
    public List<VINEntry> findAllNotFoundAndNotInUS(){
        return repo.findByVinNumberGreaterThanEqualAndVinNumberLessThanEqual(442000,520000);
    }
    @Override
    public List<VINEntry> findAllInAutocheckAndNotInNMVTIS(){
        return repo.findByAutoCheckFoundTrueAndInNmvtisFalseAndStateIsNull();
    }
    @Override
    public List<VINEntry> findAllInAutocheck(){
        return repo.findByAutoCheckFoundTrue();
    }

    @Override
    public List<VINEntry> findAllUnknownAbove(int i) {
        return repo.findByFoundFalseAndInNmvtisFalseAndCarfaxFoundFalseAndAutoCheckFoundFalseAndVinNumberGreaterThan(i);
    }
}
