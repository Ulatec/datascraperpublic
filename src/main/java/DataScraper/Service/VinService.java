package DataScraper.Service;


import DataScraper.Model.VINEntry;

import java.util.Date;
import java.util.List;


public interface VinService {
    List<VINEntry> findAll();
    void save(VINEntry vin);
    void delete(VINEntry vin);
    List<VINEntry> findAllUnknown();
    List<VINEntry> findAllKnown();
    List<VINEntry> findByVinNumberGreaterThan(int i);
    List<VINEntry> findByVin(String vin);
    List<VINEntry> findAllNotInNmvtis();
    List<VINEntry> findAllNotInCarfax();
    List<VINEntry> findByVinNumber(int i);
    List<VINEntry> findAllInCarfaxAndNotInNmvtis();
    Iterable<VINEntry> findAllTimeTest(Date date);
    List<VINEntry> findAllNotInNmvtisAndInUS();
    List<VINEntry> findAllNotFound();
    List<VINEntry> findAllInNmvtis();
    List<VINEntry> findByIdRange(int i, int j);
    List<VINEntry> findAllNotInCarfaxAndInUS();
    List<VINEntry> findAllNotInAutoCheckAndInUS();
    List<VINEntry> findAllNotFoundAndNotInUS();
    List<VINEntry> findAllInAutocheckAndNotInNMVTIS();
    List<VINEntry> findAllInAutocheck();
    List<VINEntry> findAllUnknownAbove(int i);
}
