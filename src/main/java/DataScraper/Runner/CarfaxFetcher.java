package DataScraper.Runner;


import DataScraper.Model.VINEntry;

import DataScraper.Utils.Duplicates;
import DataScraper.Utils.ListTrimmer;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.Date;
import java.util.List;

public class CarfaxFetcher extends Thread{

    private DataScraper.Service.VinService VinService;
    private List<VINEntry> vins;
    public CarfaxFetcher(DataScraper.Service.VinService vinService, List<VINEntry> vins){
        this.vins = vins;
        this.VinService = vinService;
    }

    public void run() {
        System.out.println("I AM STARTING");
        int counter = 0;
        double tested = 0;
        List<VINEntry> trimmedList = ListTrimmer.random(vins, 55);
        for(VINEntry vin : trimmedList) {
            try {
                tested = tested + 1;
                String VIN = vin.getVin();
                Document doc = Jsoup.connect("https://www.carfax.com/api/vhr-landing-page-vin-check?vin=" + VIN)
                        .header("Content-Type", "application/json")
                        .ignoreContentType(true)
                        .get();
                //Gson gson = new Gson();


                Element element = doc.body();
                JSONObject obj = new JSONObject(element.ownText());
                int numRecords = Integer.parseInt((String) obj.get("numberOfRecords"));
                if (numRecords > 0) {
                    System.out.println("found " + counter);
                    //Update Database entry.
                    vin.setCarfaxFound(true);
                    VinService.save(vin);
                    //Check database for identical VIN serials
                    Duplicates.test(VinService, vin);
                    counter = counter + 1;
                    //Log scraping progress.
                    System.out.println("[ " + new Date() + "] " + "[CANADA] " + vin.getVin() + " VIN found. Found: " + counter + " Tested: " + tested + " %: " + (tested/vins.size())*100);
                }else{
                    System.out.println("[ " + new Date() + "] " + "[CANADA] " + vin.getVin() + " VIN not found. Found: " + counter + " Tested: " + tested + " %: " + (tested/vins.size())*100);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
