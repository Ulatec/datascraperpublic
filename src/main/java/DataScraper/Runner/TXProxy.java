package DataScraper.Runner;

import DataScraper.Model.VINEntry;
import DataScraper.Service.VinService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TXProxy extends Thread{
    private VinService vinService;
    private List<VINEntry> vins;
    private String cookie;
    private String state;

    TXProxy(VinService vinService, List<VINEntry> vins, String cookie, String state){
        this.vinService = vinService;
        this.vins = vins;
        this.cookie = cookie;
        this.state = state;
    }

    public void run() {
        List<VINEntry> foundVins = new ArrayList<>();
        int counter = 0;
        double tested = 0;
        for (VINEntry vin : vins) {
            try {
                String VIN = vin.getVin();
                Document doc = Jsoup.connect("http://www.mytxcar.org/txcar_net/VehicleTestHistory.aspx?type=VIN&code=" + VIN)
                        .header("Cookie", cookie)
                        .proxy("47.41.225.7", 3128)
                        .get();
                Elements elements = doc.getElementById("resultsDiv").children();
                tested = tested + 1;
                if (elements.size() <= 2) {
                    //Log scraping progress.
                    System.out.println("[ " + new Date() + "] " + vin.getVin() + " VIN not found. Found: " + counter + " Tested: " + tested + " %: " + (tested/vins.size())*100 + " | {PROXY}");
                } else {
                    foundVins.add(vin);

                    int results = Integer.parseInt(elements.get(0).child(0).child(9).ownText().trim());
                    Element beginTime;
                    String station;
                    Element endTime;
                    if(results == 2){
                        beginTime = elements.get(4).child(0).child(1).child(0).child(0).child(0);
                        station = elements.get(4).child(0).child(1).child(2).ownText();
                        vin.setStation(elements.get(4).child(0).child(1).child(2).ownText());
                        endTime = elements.get(4).child(0).child(1).child(1);
                    }else if(results == 3){
                        beginTime = elements.get(5).child(0).child(1).child(0).child(0).child(0);
                        station = elements.get(5).child(0).child(1).child(2).ownText();
                        vin.setStation(elements.get(5).child(0).child(1).child(2).ownText());
                        endTime = elements.get(5).child(0).child(1).child(1);
                    }else{
                        beginTime = elements.get(3).child(0).child(1).child(0).child(0).child(0);
                        station = elements.get(3).child(0).child(1).child(2).ownText();
                        vin.setStation(elements.get(3).child(0).child(1).child(2).ownText());
                        endTime = elements.get(3).child(0).child(1).child(1);
                    }
                    System.out.println("results: " + results);
                    //Element stationName = elements.get(3).child(0).child(1).child(3);
                    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
                    Date date = format.parse(beginTime.ownText());
                    System.out.println("Beginning time: " + date);
                    date = format.parse(endTime.ownText());
                    System.out.println("End time: " + date);
                    System.out.println("Station ID: " + station);
                    System.out.println("VIN: " + vin.getVin());
                    for(VINEntry entry : foundVins){
                        System.out.println(entry.getVin());
                    }
                    //Set stationId
                    vin.setInspection(date);
                    vin.setDateFound(new Date());
                    vin.setState("TX");
                    //Set Found
                    vin.setFound(true);
                    //Save to DB
                    vinService.save(vin);
                    counter = counter + 1;

                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}
