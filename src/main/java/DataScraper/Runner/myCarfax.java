package DataScraper.Runner;

import DataScraper.Model.VINEntry;
import DataScraper.ThreadListiner.CanadaListener;
import DataScraper.Utils.Duplicates;
import DataScraper.Utils.ListTrimmer;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

import java.util.List;


@PropertySource("application.properties")
public class myCarfax extends Thread {
    @Autowired
    private Environment env;
    private DataScraper.Service.VinService VinService;
    private int tested;
    private int counter;
    private Instant start;
    private int size;

    public myCarfax(DataScraper.Service.VinService vinService) {
        this.VinService = vinService;
        tested = 0;
        counter = 0;
        start = Instant.now();
    }

    public void run() {
        CanadaListener listener = new CanadaListener(1);
        System.out.println("I AM STARTING");
        List<VINEntry> Vins = VinService.findAllInAutocheckAndNotInNMVTIS();
        System.out.println("size: " + Vins.size());
        size = Vins.size();
        List<VINEntry> trimmedList = ListTrimmer.random(Vins,90);
        for (VINEntry vin : trimmedList) {
            lookupByVIN(vin);
        }
        listener.threadFinish(1);
    }

    private void lookupByVIN(VINEntry vin) {

        String APITOKEN = "5c281b9b-1de2-4d27-9819-37a4cf20f74a";
        String VIN = vin.getVin();
        //Calculate time difference from start until now.
        double remainingMinutes = ((size - tested)/((double)tested/(double)Duration.between(start, Instant.now()).toMillis())*1000)/60;
        try {
            tested++;
            Connection.Response lookupResponse = Jsoup.connect("https://www.carfax.com/Service/myxapi/lookupByVin/" + VIN)
                    .header("accept", "application/json, text/plain, */*")
                    .header("accept-encoding", "gzip, deflate, br")
                    .header("accept-language", "en-US,en;q=0.9")
                    .header("cache-control", "no-cache, no-store, must-revalidate")
                    .header("csrf-token", "0tl5XIJ8-M3WHPCdJY1_NwKubwGEQnSgPenY")
                    .header("dnt", "1")
                    .header("expires", "-1")
                    .header("Pragma", "no-cache")
                    .header("referer", "https://www.carfax.com/Service/garage/172807713")
                    .header("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36 OPR/62.0.3331.132")
                    .header("x-newrelic-id", "UwAGU1NbGwIBUFVXAwMGUw==")
                    .header("Content-Type", "application/json")
                    .header("sec-fetch-site","same-origin")
                    .header("sec-fetch-mode", "cors")
                    .cookie("mycarfax", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Ik1UTXlORFkzTkRrek9tWTFaVE5qWlRFekxXRXdNVGt0TkRsbFlpMDRZek14TFRkaFlXSmlOalF6TXpCaU1BPT0iLCJpYXQiOjE1NjY3NzM4NDMsImV4cCI6Njc1MDc3Mzg0M30.f-maZMeUiuvPaXRLXJ6hcekL0eCHMKAsQh6e8g8Xbgc")
                    .cookie("api_token", APITOKEN)
                    .cookie("mbox", "PC#1567562916694-717999.17_113#1569598371|check#true#1568388831|session#1568388769289-619192#1568390631")
                    .ignoreContentType(true)
                    .execute();
            sleepThread();
            System.out.println(lookupResponse.body());
            try {
                Connection connection;
                Connection.Response vinDetailsResponse;
                connection = Jsoup.connect("https://www.carfax.com/Service/myxapi/vehicle/" + VIN)
                        .header("accept", "application/json, text/plain, */*")
                        .header("accept-encoding", "gzip, deflate, br")
                        .header("accept-language", "en-US,en;q=0.9")
                        .header("content-length", "23")
                        .header("cache-control", "no-cache, no-store, must-revalidate")
                        .header("csrf-token", "o57I7ZwS-rxorxt2YvGzbZAJD9DiVvToALwI")
                        .header("dnt", "1")
                        .header("origin", "https://www.carfax.com")
                        .header("referer", "https://www.carfax.com/Service/garage/172807713")
                        .header("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36 OPR/62.0.3331.132")
                        .header("x-newrelic-id", "UwAGU1NbGwIBUFVXAwMGUw==")
                        .header("Content-Type", "application/json;charset=UTF-8")
                        .cookie("BIGipServer~blpart~prod_chp_wl_pl", "!hrjMYz4TRO90HvBcX3ra4/bVlAuOftat8RLMFGz0YBIkyNkUHTafa6TPzNsb9xIh4gRkzp7BfnVtxw==")
                        .cookie("BIGipServer~blpart~prod_myx_int_pl", "!98X6XaZWSh2Sqb57yd0k3xx6/SANTXce5V9Cx2LjnBtXonu1IY2BFzmVrlewU+VXH3tRbgob1WP6/WQ=")
                        .cookie("CARFAXCOOKIEX", "VIN%3D" + VIN + "%23%23EMAIL%3D%23%23VEHICLE%3D2019+TESLA+MODEL+3%23%23ZIP%3D%23%23CUSTOMERTYPE%3D%23%23PARTNER%3DOPR%23%23ACCOUNTEXPIRATION%3D%23%23CHECKCOUNT%3D1%23%23CHECKVERSION%3D31%23%23LOCATION%3D0%23%23CHECKREPORT%3DDEC%23%23RECORDDISPLAYCOUNT%3D1%23%23LOCALE%3DEN")
                        .cookie("TS019fdee9", "012ae5729b8b4e35b1673442700dcf5a2120fc23b2b9352630e4c21250eee4f23f20cd1a3d4480bdd7c3f4422de490e1784190e09f30a71745ee76af01dd4dd498c1137f2bed31edc50b693022313728e11b34bdde")
                        .cookie("BIGipServer~blpart~prod_chp_int_pl", "!kGCpA5LtzZrbRPhcX3ra4/bVlAuOfmBkRv6TVTt0vwWhrRwZjYVWgsz0C3bVKFzmXD/B4VqEJ+ZfNjI=;")
                        .cookie("s_ecid", "MCMID%7C25758097218049340131738813090995114956")
                        .cookie("QSI_S_ZN_3kkfekNQa3qhakt", "v:0:0")
                        .cookie("BIGipServer~blpart~prod_mcs_int_pl", "!Ubwi5rtXHAMcFxRcX3ra4/bVlAuOfr7sJE2s1jimGeZeJQm0QOcmIMTyrbzvf31QnhMd7jTiqHFEOw==")
                        .cookie("BIGipServer~blpart~prod_myx_int_pl", "!LMNdbdds3p1841pcX3ra4/bVlAuOfkRP4PPzxnU5nO0BgMbzjnmBffSwXpgdDYcO0NDy3ZjJbuClESI=")
                        .cookie("_csrf", "KJE3RF5WcwT7t9p7Gsy6FXuy")
                        .cookie("cto_lwid", "65a1f0da-d349-486d-ac3e-e3221afcb10a")
                        .cookie("_gcl_au", "1.1.621617328.1566516276")
                        .cookie("_ga", "GA1.2.1871744798.1566516276")
                        .cookie("optimizelyEndUserId", "oeu1566516276358r0.10213773974860296")
                        .cookie("s_fid", "5135961119F014AB-1049FC202244C596")
                        .cookie("sessionID_cookie", "21437330470")
                        .cookie("_fbp", "fb.1.1566516772851.1816382939")
                        .cookie("QSI_S_SI_bxPJMe6bjt0TnGR", "r:2:1")
                        .cookie("BIGipServer~blpart~prod_cag_int_pl", "!8msrtDwMw7Da8ssgiw1SC22KoyKo6WSAb3L+TiakFsK3dR31RSxbjkstGSOYi9tP1OmrCFTnmNo2iQ==")
                        .cookie("mbox", "PC#1566516251750-510101.28_115#1567736039|check#true#1566526499|session#1566526428977-857889#1566528299")
                        .cookie("s_sess", "%20s_cc%3Dtrue%3B%20s_sq%3Dcarfaxcom%253D%252526pid%25253DOrderForm%252526pidt%25253D1%252526oid%25253Dfunctiononclick%25252528event%25252529%2525257BproductUpdate%25252528event%2525252C%2525252799.99%25252527%2525252C%252525276%25252527%2525252C%2525252760%25252527%2525252C%25252527CPP%25252527%25252529%2525253B%2525257D%252526oidt%25253D2%252526ot%25253DRADIO%2526carfaxmycarfax%253D%252526pid%25253DAdd%25252520Car%25252520Confirm%25252520Car%252526pidt%25253D1%252526oid%25253Dfunctionpr%25252528%25252529%2525257B%2525257D%252526oidt%25253D2%252526ot%25253DDIV%3B")
                        .cookie("_gid", "GA1.2.1438479653.1566756892")
                        .cookie("AMCV_AAC63BC75245B47C0A490D4D%40AdobeOrg", "1999109931%7CMCIDTS%7C18134%7CMCMID%7C25758097218049340131738813090995114956%7CMCAAMLH-1567121052%7C9%7CMCAAMB-1567361692%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCAID%7CNONE")
                        .cookie("s_cc", "true")
                        .cookie("gpv_v151", "no%20previous%20value")
                        .cookie("TS0146ef81_77", "081ace8ed5ab28009185fcf2b6ceade3f1f14f5641e7f5bb079c3c8746d32b17890f6c36429f3f8a2e5bdbc288ce9c61082f08b027823800f4a24e465e822c9507a3651400502acf209637782837e9d0622c0df144eb772037bdda0967ea12e51f938a638b6da522d7c6b7a9ef50bbb1")
                        .cookie("QSI_S_SI_bf9bVy2vQ3lOOEZ", "r:1:7")

                        .ignoreContentType(true)
                        .cookie("s_sq", "carfaxrecallwidget%3D%2526c.%2526a.%2526activitymap.%2526page%253Dhttps%25253A%25252F%25252Fwww.carfax.com%25252FService%25252Fgarage%2526link%253DYes%25252C%252520that%252527s%252520my%252520car%2526region%253Dapp%2526.activitymap%2526.a%2526.c%26carfaxmycarfax%3D%2526pid%253DAdd%252520Car%252520Confirm%252520Car%2526pidt%253D1%2526oid%253Dfunctionpr%252528%252529%25257B%25257D%2526oidt%253D2%2526ot%253DBUTTON")

                        .cookie("mycarfax", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Ik1UTXlORFkzTkRrek9tWTFaVE5qWlRFekxXRXdNVGt0TkRsbFlpMDRZek14TFRkaFlXSmlOalF6TXpCaU1BPT0iLCJpYXQiOjE1NjY3NzM4NDMsImV4cCI6Njc1MDc3Mzg0M30.f-maZMeUiuvPaXRLXJ6hcekL0eCHMKAsQh6e8g8Xbgc")
                        .cookie("TS0146ef81", "012ae5729bba98b48885e35077005df9d28400ef81be6ad99d61ed77ff1960771f0f6cb74e1b2ad639c1d28ceab28e43dd2c822bb07597990dab0eb7d130b7c7055349adfe1e1fd8f5276c9c99ef7d29c92112cc5b80843c1665dd18623ca49d34b65613e8e754e4d8e6aa18d1e962e8b6e64567198470918e0063e1721336683987a9a373594424fdc315f4cd69c2d17785ea220f6d805b4295277a943ce55de185c2f065")
                        .cookie("api_token", APITOKEN)
                        .requestBody("{\"accountId\" : 132467493}")
                        .method(Connection.Method.POST);

                vinDetailsResponse = connection.execute();

                System.out.println(vinDetailsResponse.body());

                JSONObject json = new JSONObject(vinDetailsResponse.body());
                int preEvent = json.getJSONObject("vehicle").getJSONArray("events").length();


                boolean stateFound = false;
                for (int j = 0; j < preEvent; j++) {
                    JSONArray events = json.getJSONObject("vehicle").getJSONArray("events").getJSONObject(j).getJSONArray("dashboardDetails");
                    for (int i = 0; i < events.length(); i++) {
                        JSONObject record = events.getJSONObject(i);
                        if (record.getString("label").equals("Last Registration")) {
                            String state = record.getJSONObject("displayRecord").getString("state");
                            System.out.println(state);
                            Date regDate = new SimpleDateFormat("MM/dd/yyy").parse(record.getJSONObject("displayRecord").getString("displayDate"));
                            System.out.println(regDate);
                            List<VINEntry> pulledEntries = VinService.findByVin(vin.getVin());
                            if(pulledEntries.size() == 1) {
                                VINEntry pulledEntry = pulledEntries.get(0);
                                //Update Database entry.
                                stateFound = true;
                                pulledEntry.setRegDate(regDate);
                                pulledEntry.setState(state);
                                pulledEntry.setDateFound(new Date());
                                pulledEntry.setFound(true);
                                VinService.save(pulledEntry);
                                //Check database for identical VIN serials
                                Duplicates.test(VinService, pulledEntry);
                                counter++;
                            }else {
                                System.out.println("not a single record. Multiple records found.");
                            }
                        } else {
                            System.out.println(record.getString("label"));
                        }
                    }
                }
                if (!stateFound) {
                    System.out.println("no State");
                }
                int id = json.getJSONObject("vehicle").getInt("id");
                System.out.println(id);
                sleepThread();
                Connection.Response deletionResponse;
                connection = Jsoup.connect("https://www.carfax.com/Service/myxapi/vehicle/" + id)
                        .header("accept", "application/json, text/plain, */*")
                        .header("accept-encoding", "gzip, deflate, br")
                        .header("accept-language", "en-US,en;q=0.9")
                        .header("content-length", "23")
                        .header("cache-control", "no-cache, no-store, must-revalidate")
                        .header("csrf-token", "zm53Vo3p-qxPr8s-YHxO0VcvfPje0-cdRXZY")
                        .header("dnt", "1")
                        .cookie("BIGipServer~blpart~prod_chp_wl_pl", "!hrjMYz4TRO90HvBcX3ra4/bVlAuOftat8RLMFGz0YBIkyNkUHTafa6TPzNsb9xIh4gRkzp7BfnVtxw==")
                        .cookie("BIGipServer~blpart~prod_myx_int_pl", "!98X6XaZWSh2Sqb57yd0k3xx6/SANTXce5V9Cx2LjnBtXonu1IY2BFzmVrlewU+VXH3tRbgob1WP6/WQ=")
                        .cookie("CARFAXCOOKIEX", "VIN%3D5YJ3E1EA9KF415439%23%23EMAIL%3D%23%23VEHICLE%3D2019+TESLA+MODEL+3%23%23ZIP%3D%23%23CUSTOMERTYPE%3D%23%23PARTNER%3DOPR%23%23ACCOUNTEXPIRATION%3D%23%23CHECKCOUNT%3D1%23%23CHECKVERSION%3D31%23%23LOCATION%3D0%23%23CHECKREPORT%3DDEC%23%23RECORDDISPLAYCOUNT%3D1%23%23LOCALE%3DEN")
                        .cookie("TS019fdee9", "012ae5729b8b4e35b1673442700dcf5a2120fc23b2b9352630e4c21250eee4f23f20cd1a3d4480bdd7c3f4422de490e1784190e09f30a71745ee76af01dd4dd498c1137f2bed31edc50b693022313728e11b34bdde")
                        .cookie("BIGipServer~blpart~prod_chp_int_pl", "!kGCpA5LtzZrbRPhcX3ra4/bVlAuOfmBkRv6TVTt0vwWhrRwZjYVWgsz0C3bVKFzmXD/B4VqEJ+ZfNjI=;")
                        .cookie("s_ecid", "MCMID%7C25758097218049340131738813090995114956")
                        .cookie("QSI_S_ZN_3kkfekNQa3qhakt", "v:0:0")
                        .cookie("BIGipServer~blpart~prod_mcs_int_pl", "!Ubwi5rtXHAMcFxRcX3ra4/bVlAuOfr7sJE2s1jimGeZeJQm0QOcmIMTyrbzvf31QnhMd7jTiqHFEOw==")
                        .cookie("BIGipServer~blpart~prod_myx_int_pl", "!LMNdbdds3p1841pcX3ra4/bVlAuOfkRP4PPzxnU5nO0BgMbzjnmBffSwXpgdDYcO0NDy3ZjJbuClESI=")
                        .cookie("_csrf", "9cfZ3sRK9qKbXhi9NWhl3OBX")
                        .cookie("cto_lwid", "65a1f0da-d349-486d-ac3e-e3221afcb10a")
                        .cookie("_gcl_au", "1.1.621617328.1566516276")
                        .cookie("_ga", "GA1.2.1871744798.1566516276")
                        .cookie("optimizelyEndUserId", "oeu1566516276358r0.10213773974860296")
                        .cookie("s_fid", "5135961119F014AB-1049FC202244C596")
                        .cookie("sessionID_cookie", "6186691290")
                        .cookie("_fbp", "fb.1.1566516772851.1816382939")
                        .cookie("QSI_S_SI_bxPJMe6bjt0TnGR", "r:2:1")
                        .cookie("BIGipServer~blpart~prod_cag_int_pl", "!8msrtDwMw7Da8ssgiw1SC22KoyKo6WSAb3L+TiakFsK3dR31RSxbjkstGSOYi9tP1OmrCFTnmNo2iQ==")
                        .cookie("mbox", "PC#1566516251750-510101.28_115#1567736039|check#true#1566526499|session#1566526428977-857889#1566528299")
                        .cookie("s_sess", "%20s_cc%3Dtrue%3B%20s_sq%3Dcarfaxcom%253D%252526pid%25253DOrderForm%252526pidt%25253D1%252526oid%25253Dfunctiononclick%25252528event%25252529%2525257BproductUpdate%25252528event%2525252C%2525252799.99%25252527%2525252C%252525276%25252527%2525252C%2525252760%25252527%2525252C%25252527CPP%25252527%25252529%2525253B%2525257D%252526oidt%25253D2%252526ot%25253DRADIO%2526carfaxmycarfax%253D%252526pid%25253DAdd%25252520Car%25252520Confirm%25252520Car%252526pidt%25253D1%252526oid%25253Dfunctionpr%25252528%25252529%2525257B%2525257D%252526oidt%25253D2%252526ot%25253DDIV%3B")
                        .cookie("_gid", "GA1.2.1438479653.1566756892")
                        .cookie("AMCV_AAC63BC75245B47C0A490D4D%40AdobeOrg", "1999109931%7CMCIDTS%7C18134%7CMCMID%7C25758097218049340131738813090995114956%7CMCAAMLH-1567121052%7C9%7CMCAAMB-1567361692%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCAID%7CNONE")
                        .cookie("s_cc", "true")
                        .cookie("gpv_v151", "no%20previous%20value")
                        .cookie("TS0146ef81_77", "081ace8ed5ab28009185fcf2b6ceade3f1f14f5641e7f5bb079c3c8746d32b17890f6c36429f3f8a2e5bdbc288ce9c61082f08b027823800f4a24e465e822c9507a3651400502acf209637782837e9d0622c0df144eb772037bdda0967ea12e51f938a638b6da522d7c6b7a9ef50bbb1")
                        .cookie("QSI_S_SI_bf9bVy2vQ3lOOEZ", "r:1:7")
                        .cookie("mycarfax", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Ik1UTXlORFkzTkRrek9tWTFaVE5qWlRFekxXRXdNVGt0TkRsbFlpMDRZek14TFRkaFlXSmlOalF6TXpCaU1BPT0iLCJpYXQiOjE1NjY3NzM4NDMsImV4cCI6Njc1MDc3Mzg0M30.f-maZMeUiuvPaXRLXJ6hcekL0eCHMKAsQh6e8g8Xbgc")
                        .cookie("TS0146ef81", "012ae5729bba98b48885e35077005df9d28400ef81be6ad99d61ed77ff1960771f0f6cb74e1b2ad639c1d28ceab28e43dd2c822bb07597990dab0eb7d130b7c7055349adfe1e1fd8f5276c9c99ef7d29c92112cc5b80843c1665dd18623ca49d34b65613e8e754e4d8e6aa18d1e962e8b6e64567198470918e0063e1721336683987a9a373594424fdc315f4cd69c2d17785ea220f6d805b4295277a943ce55de185c2f065")
                        .cookie("s_sq", "carfaxrecallwidget%3D%2526c.%2526a.%2526activitymap.%2526page%253Dhttps%25253A%25252F%25252Fwww.carfax.com%25252FService%25252Fgarage%2526link%253DYes%25252C%252520that%252527s%252520my%252520car%2526region%253Dapp%2526.activitymap%2526.a%2526.c%26carfaxmycarfax%3D%2526pid%253DAdd%252520Car%252520Confirm%252520Car%2526pidt%253D1%2526oid%253Dfunctionpr%252528%252529%25257B%25257D%2526oidt%253D2%2526ot%253DBUTTON")
                        .header("origin", "https://www.carfax.com")
                        .header("referer", "https://www.carfax.com/Service/garage")
                        .header("expires", "-1")
                        .header("Pragma", "no-cache")
                        .header("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36 OPR/62.0.3331.132")
                        .header("x-newrelic-id", "UwAGU1NbGwIBUFVXAwMGUw==")
                        .cookie("api_token", APITOKEN)
                        .ignoreContentType(true)
                        .method(Connection.Method.DELETE);

                deletionResponse = connection.execute();
                System.out.println(deletionResponse.body() + "end");
                //Log scraping progress.
                System.out.println("[ " + new Date() + "] " + "[myCarfax] " + vin.getVin() + "  Found: " + counter + " Tested: " + tested + " %: " + ((double) tested / (double) size) * 100 + " Remaining: " + (int)remainingMinutes) ;
                sleepThread();
            } catch (HttpStatusException e) {
                System.out.println(e.getStatusCode());
                System.out.println(e.getMessage());
                System.out.println(e.toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sleepThread(){
        try {
            int rand = (int) (Math.random() * 2175);
            sleep(2650 + rand);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
