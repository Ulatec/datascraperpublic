package DataScraper.Runner;

import DataScraper.Model.VINEntry;
import DataScraper.Service.VinService;
import DataScraper.Utils.IPLISTS;
import DataScraper.Utils.ListSplitter;

import java.util.ArrayList;
import java.util.List;

public class CarJamRunner extends Thread {

    private VinService vinService;
    public CarJamRunner(VinService vinService){
        this.vinService = vinService;
    }
    public void run() {
        List<String> IPs = IPLISTS.AllIPs;


        int numThreads = IPs.size();
        List<VINEntry> Vins = vinService.findAllNotFoundAndNotInUS();
        //Split list of all VINs into multiple lists. One list for each thread.
        ArrayList<List<VINEntry>> lists = ListSplitter.split(Vins, numThreads, 0);

        for (int i = 0; i < numThreads; i++) {
            //initiate a new thread for each proxy IP.
            CarJamFetcher fetcher = new CarJamFetcher(vinService, lists.get(i), IPs.get(i), i);
            fetcher.start();
        }

    }
}
