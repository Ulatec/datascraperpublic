package DataScraper.Runner;

import DataScraper.Model.VINEntry;
import DataScraper.ThreadListiner.NMVTISListener;
import DataScraper.Utils.Duplicates;
import DataScraper.Utils.ListTrimmer;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class NMVTISProxy extends Thread{

    private DataScraper.Service.VinService VinService;
    private String IP;
    private List<VINEntry> vins;
    private NMVTISListener nmvtisListener;
    private int threadNum;
    private int paymentForms;
    private int instavinsuccess;
    private int vinauditFailure;
    private int failures;
    public NMVTISProxy(DataScraper.Service.VinService vinService, List<VINEntry> vins, String IP,NMVTISListener nmvtisListener, int threadNum){
        this.VinService = vinService;
        this.IP = IP;
        this.vins = vins;
        this.threadNum = threadNum;
        this.nmvtisListener = nmvtisListener;
    }

    public void run() {
        Instant start = Instant.now();
        paymentForms = 0;
        instavinsuccess = 0;
        vinauditFailure = 0;
        failures = 0;
        int counter = 0;
        int tested = 0;
        List<VINEntry>trimmedVins = ListTrimmer.random(vins, 60);
        Collections.shuffle(trimmedVins);
        for(VINEntry vin : trimmedVins) {
            int rand = (int) (Math.random()*551);
            boolean found = false;
            //Random a number to select which site to check each vin for.
            if(rand<130) {
                found = CheckThatVin(vin);
            }else if(rand<=254 && rand>130) {
                found = ClearVin(vin);
            }else if(rand<=391 && rand>254) {
                found = VinSmart(vin);
            }else if(rand<=539 && rand>391){
                found = VinGurus(vin);
            }else if(rand<=551 && rand>538){
                found = VinAudit(vin);
            }
            tested = tested + 1;
            Date currentTime = new Date();
            //Calculate time difference from start until now.
            double remainingMinutes = ((trimmedVins.size() - tested)/((double)tested/(double)Duration.between(start, Instant.now()).toMillis())*1000)/60;
            if(found) {
                //If VIN was found at the site in which it was tested, update its record.
                System.out.println("VIN: " + vin.getVin());
                List<VINEntry> pulledEntries = VinService.findByVin(vin.getVin());
                if (pulledEntries.size() == 1) {
                    counter = counter + 1;
                    VINEntry pulledEntry = pulledEntries.get(0);
                    //Update Database entry.
                    pulledEntry.setNMVTISDate(new Date());
                    pulledEntry.setInNMVTIS(true);
                    VinService.save(pulledEntry);
                    //Check database for identical VIN serials
                    Duplicates.test(VinService, pulledEntry);
                } else {
                    System.out.println("not a single record. Multiple records found.");
                }

                System.out.println("[ " + currentTime + "] " + "[NMVTIS] " + vin.getVin() + " VIN FOUND. Found: " + counter + " Tested: " + tested + " %: " + (((double) tested/ (double)trimmedVins.size()))*100 + "payment: " + paymentForms + "instavin success:" + instavinsuccess + " minutes: " + (int)remainingMinutes);
            }else{
                System.out.println("[ " + currentTime + "] " + "[NMVTIS] " + "[" + threadNum + "] " + vin.getVin() + " VIN not found. Found: " + counter + " Tested: " + tested + " %: " + (((double) tested/ (double)trimmedVins.size()))*100 + " payment: " + paymentForms + " instavin success:" + instavinsuccess + "Failures="+failures+":"+  " minutes: " + (int)remainingMinutes);
            }

            try {
                sleep(6425 + (int) (Math.random()*2325));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        nmvtisListener.threadFinish(threadNum);
    }


    private boolean CheckThatVin(VINEntry vin){
        String VIN = vin.getVin();
        try {
            Document doc = Jsoup.connect("https://www.checkthatvin.com/?page_frame=blank&action=newctv&cmd=18&vin=" + VIN)
                    .ignoreContentType(true)
                    .timeout(32000)
                    .proxy(IP, 3128)
                    .get();
            Element element = doc.body();
            JSONObject obj = new JSONObject(element.ownText());

            return (boolean) obj.get("PRECHECK");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("problem with: " + IP);
            failures++;
            return false;
        }
    }

    private boolean ClearVin(VINEntry vin){

        String VIN = vin.getVin();
        try {
            Document doc = Jsoup.connect("https://www.clearvin.com/report/verify")
                    .header("Content-type", "application/x-www-form-urlencoded")
                    .data("vin", VIN)
                    .ignoreContentType(true)
                    .proxy(IP, 3128)
                    .post();

            Element element = doc.body();
            JSONObject obj = new JSONObject(element.ownText());

            return (boolean) obj.get("exist");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("problem with: " + IP);
            failures++;
            return false;
        }
    }
    private boolean InstaVin(VINEntry vin){

        try{
            String VIN = vin.getVin();
            String body = "{\"mode\":0,\"vin\":\"" + VIN + "\",\"captcha\":\"\"}";
            System.out.println(body);
            Connection.Response doc = Jsoup.connect("https://www.instavin.com/ca-api/order/product?offerName=NMVTIS")
                    .header("Content-type","application/json; charset=utf-8")
                    .requestBody(body)
                    .ignoreContentType(true)
                    .proxy(IP, 3128)
                    .followRedirects(true)
                    .timeout(34000)
                    .method(Connection.Method.POST)
                    .execute();

            Element element = doc.parse().body();
            JSONObject obj = new JSONObject(element.ownText());
            JSONObject obj2 = obj.getJSONObject("payload").getJSONObject("vinCheckResult");
            instavinsuccess = instavinsuccess + 1;
            return (boolean) obj2.get("isDataAvailable");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("problem with: " + IP);
            failures++;
            return false;
        }
    }
    private boolean VinSmart(VINEntry vin){
        String VIN = vin.getVin();
        try {
            Document doc = Jsoup.connect("https://www.vinsmart.com/")
                    .ignoreContentType(true)
                    .data("__EVENTTARGET","ctl00$ctl00$MainContent$ucCheckVIN_3$lnkGetREport")
                    .data("__LASTFOCUS", "")
                    .data("__EVENTARGUMENT", "")
                    .data("__VIEWSTATE", "rmAMUlKctPULQV4Qg0+kZj86qep+PQBIyTLdYKINkNZiYPZHwGYBPoIzpsCLRP1O1QnUz9ZdjV65EzcPF6c+KYXITlmzxCZMD3SqcEq4aTxaL3CF8MtXnpWGDwwYAMAKgMgcQB8KWnjSZc+iLMMcKtYdFsj3VHjNspUI1Q+1DAncAjkVAAWKPlT0XbsCP7ltE387YqrMyGtv+YaOJ3zWG/N/wQYwBJ6YKB/SrpmDK22jDiBCWvDffw2RsKJpShU/tSawz6YrxIuo3ok81Zyx5Q==")
                    .data("ctl00$ctl00$MainContent$ucCheckVIN_3$vins",VIN)
                    .data("__VIEWSTATEGENERATOR", "CA0B0334")
                    .proxy(IP, 3128)
                    .get();

            Element element = doc.body();
            Element testElement = element.getElementById("MainContent_ucPayNR_3_txtVIN");

            return testElement != null;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("problem with: " + IP);
            failures++;
            return false;
        }
    }
    private boolean VinGurus(VINEntry vin){
        String VIN = vin.getVin();
        try {
            Connection.Response res = Jsoup.connect("https://www.vingurus.com/en/landing/checkout?vin=" + VIN)
                    .proxy(IP, 3128)
                    .followRedirects(true)
                    .method(Connection.Method.GET)
                    .execute();

            Document doc = res.parse();
            Element element = doc.body();
            Element paymentForm = element.getElementById("payment-form");

            if(paymentForm != null){
                paymentForms = paymentForms + 1;
            }
            return paymentForm != null;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("problem with: " + IP);
            failures++;
            return false;
        }
    }
    private boolean VinAudit(VINEntry vin){
        String VIN = vin.getVin();
        try {
            Document doc = Jsoup.connect("https://api.vinaudit.com/query.php?vin=" + VIN + "&key=VA_MAIN&try=0&mode=&rd=0.9071039140545893")
                    .proxy(IP, 3128)
                    .timeout(32000)
                    .ignoreContentType(true)
                    .get();
            JSONObject json = new JSONObject(doc.body().ownText());
            return (boolean) json.get("success");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("VinAudit: problem with: " + IP);
            vinauditFailure++;
            failures++;
            return false;
        }
    }

}
