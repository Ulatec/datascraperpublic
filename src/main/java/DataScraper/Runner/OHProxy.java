package DataScraper.Runner;

import DataScraper.Model.VINEntry;
import DataScraper.Service.VinService;
import DataScraper.ThreadListiner.OHListener;
import DataScraper.Utils.Duplicates;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class OHProxy extends Thread{
    private VinService vinService;
    private List<VINEntry> vins;
    private String cookie;
    private String state;
    private String IP;
    private int threadIndex;
    private OHListener ohListener;

    public OHProxy(VinService vinService, List<VINEntry> vins, String IP, OHListener ohListener, int index){
        this.vins = vins;
        this.vinService = vinService;
        this.IP = IP;
        this.threadIndex = index;
        this.ohListener = ohListener;
    }
    public void run() {
        System.out.println("PROXY THREAD STARTING");
        int counter = 0;
        double tested = 0;
        //Query limit before sleeping.
        int limit = 295;
        Collections.shuffle(vins);
        for (VINEntry vin : vins) {
            try {
                String VIN = vin.getVin();
                Document doc = Jsoup.connect("https://services.dps.ohio.gov/BMVOnlineServices/Search/Title?SearchTerm=" + VIN + "&action=Search+VIN")
                        .header("Content-type", "application/x-www-form-urlencoded")
                        .proxy(IP, 3128)
                        .post();
                Elements errorElements = doc.getElementsByClass("validation-summary-errors");
                tested = tested + 1;
                if (errorElements.size() == 0) {
                    List<VINEntry> pulledEntries = vinService.findByVin(vin.getVin());
                    if(pulledEntries.size() == 1) {
                        Elements tdElements = doc.getElementsByTag("td");
                        Date date = new SimpleDateFormat("MM/dd/yyyy").parse(tdElements.get(17).ownText());
                        VINEntry pulledEntry = pulledEntries.get(0);
                        //Update Database entry.
                        pulledEntry.setPrice(tdElements.get(37).ownText());
                        pulledEntry.setFound(true);
                        pulledEntry.setState("OH");
                        pulledEntry.setRegDate(date);
                        pulledEntry.setDateFound(new Date());
                        vinService.save(pulledEntry);
                        //Check database for identical VIN serials
                        Duplicates.test(vinService, pulledEntry);
                    }else{
                        System.out.println("not a single record. Multiple records found.");
                    }
                    counter = counter + 1;
                    //Log scraping progress.
                    System.out.println("[ " + new Date() + "] " +  "[P" + threadIndex + "] " + vin.getVin() + " VIN found! Found: " + counter + " Tested: " + tested + " %: " + (tested / vins.size())*100);
                } else {
                    System.out.println("[ " + new Date() + "] " + "[P" + threadIndex + "] " + vin.getVin() + " VIN not found. Found: " + counter + " Tested: " + tested + " %: " + (tested / vins.size())*100);
                }
                if(tested%limit==0 && tested!= 0){
                    //wait 5+ minutes due to server request restrictions.
                    sleep(325000);
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(threadIndex + " has a problem. IP: " + IP);
            }
        }
        ohListener.threadFinish(threadIndex);
    }
}
