package DataScraper.Runner;

import DataScraper.Model.VINEntry;
import DataScraper.Service.VinService;
import DataScraper.ThreadListiner.OHListener;

import DataScraper.Utils.IPLISTS;
import DataScraper.Utils.ListSplitter;

import java.util.*;

public class OHRunner extends Thread{
    private VinService vinService;


    public OHRunner(VinService vinService){
        this.vinService = vinService;
    }

    public void run() {
        List<VINEntry> Vins = vinService.findAllUnknown();
        System.out.println("Unknown VINS: " + Vins.size());

        List<String> IPs = IPLISTS.AllIPs;
        int numThreads = IPs.size();
        //Split list of all VINs into multiple lists. One list for each thread.
        ArrayList<List<VINEntry>> lists = ListSplitter.split(Vins, numThreads, 0);
        OHListener listener = new OHListener(numThreads);
        for(int i=0; i <numThreads; i++ ){
            //initiate a new thread for each proxy IP.
            OHProxy fetcher = new OHProxy(vinService, lists.get(i), IPs.get(i), listener, i);
            fetcher.start();
        }

    }
}
