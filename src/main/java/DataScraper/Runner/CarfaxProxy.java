package DataScraper.Runner;

import DataScraper.Model.VINEntry;
import DataScraper.Utils.Duplicates;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class CarfaxProxy extends Thread{

    private DataScraper.Service.VinService VinService;
    private String IP;
    private List<VINEntry> vins;
    private int threadNum;
    public CarfaxProxy(DataScraper.Service.VinService vinService, List<VINEntry> vins, String IP, int threadNum){
        this.VinService = vinService;
        this.IP = IP;
        this.vins = vins;
        this.threadNum = threadNum;
    }

    public void run() {
        Instant start = Instant.now();
        System.out.println("I AM STARTING");
        int counter = 0;
        int tested = 0;
        vins = trimList(vins);
        System.out.println(vins.size());
        Collections.shuffle(vins);
        for(VINEntry vin : vins) {
            try {
                tested = tested + 1;
                String VIN = vin.getVin();
                Document doc = Jsoup.connect("https://www.carfax.com/api/vhr-landing-page-vin-check?vin=" + VIN)
                        .header("Content-Type", "application/json")

                        .ignoreContentType(true)
                        .proxy(IP, 3128)
                        .get();

                //Calculate time difference from start until now.
                double remainingMinutes = ((vins.size() - tested)/((double)tested/(double)Duration.between(start, Instant.now()).toMillis())*1000)/60;
                Element element = doc.body();
                try {
                    JSONObject obj = new JSONObject(element.ownText());
                    int numRecords = Integer.parseInt((String) obj.get("numberOfRecords"));
                    if (numRecords > 0) {
                        List<VINEntry> pulledEntries = VinService.findByVin(vin.getVin());
                        if(pulledEntries.size() == 1) {
                            VINEntry pulledEntry = pulledEntries.get(0);
                            //Update Database entry.
                            pulledEntry.setCarfaxFound(true);
                            pulledEntry.setCarfaxFoundDate(new Date());
                            VinService.save(pulledEntry);
                            Duplicates.test(VinService, pulledEntry);
                        }else {
                            System.out.println("not a single record. Multiple records found.");
                        }
                        System.out.println("found " + counter);
                        counter = counter + 1;
                        //Log scraping progress.
                        System.out.println("[ " + new Date() + "] " + "[P" + threadNum + "] " + " [Carfax] " + vin.getVin() + " VIN found! Found: " + counter + " Tested: " + tested + " %: " + ((double)tested / (double)vins.size()) * 100 + " Remaining: " + (int)remainingMinutes);
                    } else {
                        System.out.println("[ " + new Date() + "] " + "[P" + threadNum + "] " + " [Carfax] " + vin.getVin() + " VIN not found. Found: " + counter + " Tested: " + tested + " %: " + ((double)tested / (double)vins.size()) * 100 + " Remaining: " + (int)remainingMinutes);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                    System.out.println(threadNum + " has a problem. IP: " + IP);
                }

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(threadNum + " has a problem. IP: " + IP);
            }
            try {
                int rand = (int) (Math.random()*925);
                sleep(5000 + rand);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    private static String cookie = "BIGipServer~blpart~prod_chp_int_pl=!MoPRqdev9c81XsJcX3ra4/bVlAuOfnoxjoFKwuoT9DUFHnWQlq1xjGAKCwabn8jgpBa7wc+hr29mscI=; _gcl_au=1.1.1410185974.1567562899; sessionID_cookie=21437330470; _ga=GA1.2.1566648196.1567562899; s_fid=125520AEBF4A3717-1395700606BD5C8F; _fbp=fb.1.1567562898832.2034584550; s_ecid=MCMID%7C60130918085873671803556417712171726257; BIGipServer~blpart~prod_mcs_int_pl=!Jg6usL/3SbyhqPJcX3ra4/bVlAuOftWWX3eHhtZO0wnfDSq03+Cp1t0tZ6OR9WcSs/V5wJE4FQ+pSQ==; BIGipServer~blpart~prod_sso_int_pl=!yHQvxTmAd1kuhZFcX3ra4/bVlAuOflTaLaSAc8G8W3CFQR7qRBKlLNijPmA2/eIr2cSbx7HHGjVmUQs=; BIGipServer~blpart~prod_cag_int_pl=!I2zhpo5JN/dmsQxcX3ra4/bVlAuOfv7YQMrRxd+MijTQjxY8c20tnS4GufsRlKCOm/CYhcJS1thOFiQ=; BIGipServer~blpart~prod_cop_int_pl=!wFl6NxvBxhLSDqVcX3ra4/bVlAuOfhPncQ8TeUfcs0+UQAPmUudQfAAD40DyM56SZgVkHt7ZF5kYEQ==; _csrf=KJE3RF5WcwT7t9p7Gsy6FXuy; BIGipServer~blpart~prod_myx_int_pl=!yAE0m+j+dDvVuClcX3ra4/bVlAuOfkBHzF3IvqczBgRnG0jnXBvmmi9Gfsrqiy/VpMXB6SFeKlOyD28=; cto_lwid=79a2c268-f258-4793-9d7d-aa35362aca00; optimizelyEndUserId=oeu1567562961653r0.4037080524749901; BIGipServer~blpart~prod_ars_int_pl=!29s45t18uCpVKglcX3ra4/bVlAuOfnjQYdQaqIxZ7mfTx/Cs4AGXbgPvB0gV1+kBKuXikTOVdkGR4A==; mycarfax=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Ik1UTXlORFkzTkRrek9qWXlNbVpsTURkbExUY3laVEl0TkRoak1DMDVOamhoTFdVeU16UmxZbVUzTUdObFlnPT0iLCJpYXQiOjE1Njc3ODIzNTQsImV4cCI6Njc1MTc4MjM1NH0.EhVNfuktV35iYQcaHVaOqLDa7-bfzoq9N8LHg0PtGs8; BIGipServer~blpart~prod_icr_int_pl=!sPC2eQTeWihNDZJcX3ra4/bVlAuOfsaU1eWjefwLnVS5m1DwyoqkGB30Kb+AgfX73Wbb+40MeiQilQ==; QSI_HistorySession=https%3A%2F%2Fwww.carfax.com%2F~1568388768170%7Chttps%3A%2F%2Fwww.carfax.com%2FService%2Fgarage%2F179272593~1569849580148; TS0178b56b=012ae5729b3074130e6854cc9aa6289298994c49625e0623245fde118af1b3962bc3e11c8f94b2d7aba1944e2a9609f922ed61f22fcbad835d0bbc6cee359656445135d459; PHPSESSID=nar1c2ple386v0otb8qipqqoq1; BIGipServer~blpart~prod_aws_blg_pl=!4zzh586vXDqdaNP+qse3oRyOCwf1PihGukzCSbi078bHt7OGJqPPC9omwFH4m1e2w0xsJkMkUtSom28=; QSI_S_SI_bf9bVy2vQ3lOOEZ=r:1:19; BIGipServer~blpart~prod_ucl_int_pl=!0beUSzFCwf9VYvR7yd0k3xx6/SANTXSiFQ5+qsUL/EJUte9x1dW05IgUXzrMFTnb4mJKGSkQoVtElU4=; AMCVS_AAC63BC75245B47C0A490D4D%40AdobeOrg=1; AMCV_AAC63BC75245B47C0A490D4D%40AdobeOrg=1406116232%7CMCIDTS%7C18183%7CMCMID%7C60130918085873671803556417712171726257%7CMCAAMLH-1571004632.705%7C9%7CMCAAMB-1571004691%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCAID%7CNONE%7CMCOPTOUT-1571011892s%7CNONE%7CvVersion%7C2.5.0; uuid=326382fb-e203-41da-9c5f-c915c157b9b3; s_sess=%20s_sq%3Dcarfaxcom%253D%252526pid%25253DLogin%25252520Main%252526pidt%25253D1%252526oid%25253DSign%25252520In%252526oidt%25253D3%252526ot%25253DSUBMIT%2526carfaxrecallwidget%253D%252526c.%252526a.%252526activitymap.%252526page%25253Dhttps%2525253A%2525252F%2525252Fwww.carfax.com%2525252FService%2525252F%252526link%25253DSign%25252520In%252526region%25253Dcarfax-global-header%252526.activitymap%252526.a%252526.c%3B%20s_cc%3Dtrue%3B; id=247825313; en=p; s_sq=carfaxcom%3D%2526c.%2526a.%2526activitymap.%2526page%253DFind%252520Car%252520-%252520Main%252520Page%2526link%253DCARFAX%252520Reports%2526region%253Dresponsive-menu%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253DFind%252520Car%252520-%252520Main%252520Page%2526pidt%253D1%2526oid%253Dhttps%25253A%25252F%25252Fwww.carfax.com%25252Fvehicle-history-reports%25252F%2526ot%253DA%26carfaxrecallwidget%3D%2526c.%2526a.%2526activitymap.%2526page%253Dhttps%25253A%25252F%25252Fwww.carfax.com%25252FService%25252Fgarage%25252F180158433%2526link%253DYes%25252C%252520Delete%2526region%253Dgarage-edit-bar%2526.activitymap%2526.a%2526.c; s_cc=true; TS0146ef81_77=081ace8ed5ab28007a3a779809952a6b6dc7460965ac922b6bb3074ed8932fafa1ea9974e57cd3322ae8bd20001faff808693f6d258238001c858f937830c6857eb4f3886e3e4aac4e77e94b85d2a231fea113c2867a63e80466e838394b823daaddbebb7dbc5e757fec24b017c1c4e3; TS0146ef81=012ae5729b71ec9a8e85a782474e199cee46210968340cc289cd537df6dda8882f7f5a1d3c60a76adb3b0b7ba6ecf492aa1b00b794146322e43a7f6a26ac56ff0bf1d0c62c92a609b103ec5f7c536e8ab3f57985943db3da6539926be995788183b13d51f1c7c0d377f1c7f85fe98b9d742355587730d5967a71be4bf794b563e42826121db89493995c04db79200d8ee274755f8e758d37437409424e1ad6361a252cdb22c8da1ff480193b0185d2609aba92d565632f990cc7d9b56bbc601357720044bd539dd597fc9d048922aea4e245f63ea69691387135e7d4104320386c6046daee8f4df27ffc695c1ba2347c4150622b0f; mbox=PC#1569847362476-8835.17_60#1572223776|session#1571013955567-535788#1571016036|em-disabled#true#1571015766|check#true#1571014236; datadome=9JMkk~c1jZD3spvIEuJXpkauFU9CkcdN14NxYx.ExF6sukj4~5hK66OTOj1fF.x4nRGBvJcq967yVy2ZYqhW-x94sw.b.b8Rs.dNPbMy-o; TS01f16f44=012ae5729b83ae652161301047c8905620661d7a42974c41c16f3b86a2c4f152c42fdf89a21bae81c705f4f6d9091b7e7c2d997190af51b866161ebf02d064df6a962f76ebd612f145ee0a10fdcb7168ddb9ba411b25976cf409319fbccd57d65764b67536a129eb00ccb8c811a8147d136b6ad66c9696d4b66d80add3e3c95ff9be73dfe7967e8b3105e115924690921979cf03d4a7a9db9bc89db2c1ae5282440737a99893f8dd6bd93f2da6ec4ffeb5b7b7936d43d44f8913add5e29d222e8e1085ea47466dd1f3e75b8d99532b2beaa890cebb27d13cc238784193d917c5a96e612b1a";


    private List<VINEntry> trimList(List<VINEntry> vins){
        int size = vins.size();
        System.out.println("start size: " + size);
        int finalSize = (int) (vins.size()*0.62);
        for(int i = vins.size() - 1; i> finalSize; i--){
            int rand = (int)(Math.random()*i);
            vins.remove(rand);
        }
        System.out.println("new size:" + vins.size());
        return vins;
    }
}
