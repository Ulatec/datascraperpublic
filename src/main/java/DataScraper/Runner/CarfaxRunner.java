package DataScraper.Runner;


import DataScraper.Model.VINEntry;
import DataScraper.Service.VinService;
import DataScraper.Utils.IPLISTS;
import DataScraper.Utils.ListSplitter;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class CarfaxRunner extends Thread {

    private VinService VinService;

    public CarfaxRunner(VinService vinService){
        this.VinService = vinService;
    }
    public void run(){
        List<VINEntry> Vins = VinService.findAllNotInCarfaxAndInUS();
        System.out.println("Unknown VINS: " + Vins.size());


        List<String> IPs = IPLISTS.CarfaxIPs;

        int numThreads= 2;
        ArrayList<List<VINEntry>> lists = ListSplitter.split(Vins, numThreads, 0);
        for(int i=0; i <numThreads; i++ ){
            //initiate a new thread for each proxy IP.
            CarfaxProxy fetcher = new CarfaxProxy(VinService, lists.get(i), IPs.get(i), i);
            fetcher.start();
        }
    }


}
