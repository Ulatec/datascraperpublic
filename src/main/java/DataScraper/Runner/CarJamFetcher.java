package DataScraper.Runner;

import DataScraper.Model.VINEntry;
import DataScraper.Service.VinService;
import DataScraper.Utils.Duplicates;
import DataScraper.Utils.ListTrimmer;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;

public class CarJamFetcher extends Thread {

    private VinService vinService;
    private List<VINEntry> vins;
    private String IP;
    private int thread;
    public CarJamFetcher(VinService vinService,List<VINEntry> vins,String IP, int thread){
        this.vinService = vinService;
        this.vins = vins;
        this.IP = IP;
        this.thread = thread;
    }

    public void run(){
        Instant start = Instant.now();
        int counter = 0;
        int tested = 0;
        List<VINEntry> trimmedList = ListTrimmer.random(vins, 25);
        for(VINEntry vin : trimmedList) {
            try {
                String AussieVIN = vin.getVin().replace("5YJ3E1E", "5YJ3F7E");
                Jsoup.connect("https://www.carjam.co.nz/car/?plate=" + AussieVIN)
                        .proxy(IP, 3128)
                        .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36 OPR/63.0.3368.55666")
                        .method(Connection.Method.GET)
                        .followRedirects(true)
                        .execute();

                sleepThread();
                //CarJam takes a bit to populate a Vin on their front end. Requesting a second time should yield results.
                Connection.Response res = Jsoup.connect("https://www.carjam.co.nz/car/?plate=" + AussieVIN)
                        .proxy(IP, 3128)
                        .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36 OPR/63.0.3368.55666")
                        .method(Connection.Method.GET)
                        .followRedirects(true)
                        .execute();

                tested++;
                Document doc = res.parse();
                Elements eles = doc.getElementsByAttributeValue("data-key", "date_of_first_registration_in_nz");

                //Calculate time difference from start until now.
                double remainingMinutes = ((trimmedList.size() - tested)/((double)tested/(double)Duration.between(start, Instant.now()).toMillis())*1000)/60;
                if (eles.size() > 0) {
                    List<VINEntry> pulledEntries = vinService.findByVin(vin.getVin());
                    if(pulledEntries.size() == 1){
                        counter++;
                        VINEntry pulledEntry = pulledEntries.get(0);
                        //Update Database entry.
                        pulledEntry.setDateFound(new Date());
                        pulledEntry.setNonUS(true);
                        pulledEntry.setFound(true);
                        Element ele = eles.next().first();
                        Date tempDate = new SimpleDateFormat("dd-MMM-yyyy").parse(ele.ownText());
                        System.out.println(tempDate);
                        pulledEntry.setCountry("NZ");
                        pulledEntry.setRegDate(tempDate);
                        pulledEntry.setVin(AussieVIN);
                        vinService.save(pulledEntry);
                        //Check database for identical VIN serials
                        Duplicates.test(vinService, pulledEntry);
                        //Log scraping progress.
                        System.out.println("[ " + new Date() + "] " + "[CarJam] " + "[P" + thread + "] " + AussieVIN + " VIN found. Found: " + counter + " Tested: " + tested + " %: " + ((double) tested / (double) trimmedList.size()) * 100 + " Remaining: " + (int)remainingMinutes) ;
                    }
                }else{
                    System.out.println("[ " + new Date() + "] " + "[CarJam] " + "[P" + thread + "] " + AussieVIN + " VIN found. Found: " + counter + " Tested: " + tested + " %: " + ((double) tested / (double) trimmedList.size()) * 100 + " Remaining: " + (int)remainingMinutes) ;

                }
                sleepThread();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private void sleepThread() throws InterruptedException {
        sleep( 8700 + (int) (Math.random()*6950));
    }
}
