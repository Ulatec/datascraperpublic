package DataScraper.Runner;

import DataScraper.Model.VINEntry;
import DataScraper.Service.VinService;
import DataScraper.ThreadListiner.AutocheckListener;
import DataScraper.Utils.Duplicates;
import DataScraper.Utils.ListTrimmer;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;

public class AutoCheckFetcher extends Thread{
    private VinService vinService;
    private int threadNum;
    private List<VINEntry> vins;
    private String IP;
    private AutocheckListener listener;
    public AutoCheckFetcher(VinService vinService, List<VINEntry> vins, String IP, AutocheckListener listener, int threadNum) {
        this.vinService = vinService;
        this.threadNum = threadNum;
        this.vins = vins;
        this.IP = IP;
        this.listener = listener;
    }

    public void run(){
        Instant start = Instant.now();
        int counter = 0;
        int tested = 0;
        List<VINEntry> trimmedList = ListTrimmer.random(vins, 70);
        for(VINEntry vin : trimmedList) {
            try {

                Connection.Response autocheckResponse = Jsoup.connect("https://www.autocheck.com/consumer-api/meta/v1/summary/" + vin.getVin())
                        .proxy(IP, 3128)
                        .method(Connection.Method.GET)
                        .ignoreContentType(true)
                        .execute();
                tested++;
                if (autocheckResponse.statusCode() == 200) {
                    Document doc = autocheckResponse.parse();
                    JSONObject json = new JSONObject(doc.body().ownText().replace("[", "").replace("]", ""));

                    //Calculate time difference from start until now.
                    double remainingMinutes = ((trimmedList.size() - tested) / ((double) tested / (double) Duration.between(start, Instant.now()).toMillis()) * 1000) / 60;

                    if ((int) json.get("recordCount") > 0) {
                        List<VINEntry> pulledEntries = vinService.findByVin(vin.getVin());
                        if (pulledEntries.size() == 1) {
                            VINEntry pulledEntry = pulledEntries.get(0);
                            //Update Database entry.
                            pulledEntry.setAutoCheckFound(true);
                            pulledEntry.setAutoCheckDate(new Date());
                            vinService.save(pulledEntry);
                            //Check database for identical VIN serials
                            Duplicates.test(vinService, pulledEntry);
                        } else {
                            System.out.println("not a single record. Multiple records found.");
                        }
                        counter++;

                        //Log scraping progress.
                        System.out.println("[ " + new Date() + "] " + "[AutoCheck] " + "[P" + threadNum + "] " + vin.getVin() + " VIN found. Found: " + counter + " Tested: " + tested + " %: " + ((double) tested / (double) trimmedList.size()) * 100 + " Remaining: " + (int) remainingMinutes);
                    } else {
                        System.out.println("[ " + new Date() + "] " + "[AutoCheck] " + "[P" + threadNum + "] " + vin.getVin() + " VIN not found. Found: " + counter + " Tested: " + tested + " %: " + ((double) tested / (double) trimmedList.size()) * 100 + " Remaining: " + (int) remainingMinutes);
                    }
                }
                } catch(Exception e){
                    e.printStackTrace();
                    System.out.println("Thread #" + threadNum + " Has a problem. IP Address: " + IP);
                }
                try {
                    int rand = (int) (Math.random() * 1025);
                    sleep(1250 + rand);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        listener.threadFinish(threadNum);
        }
}


