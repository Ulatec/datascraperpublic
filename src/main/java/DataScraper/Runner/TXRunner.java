package DataScraper.Runner;

import DataScraper.Model.VINEntry;
import DataScraper.Service.VinService;
import DataScraper.ThreadListiner.TXListener;
import DataScraper.Utils.IPLISTS;
import DataScraper.Utils.ListSplitter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.util.*;

@PropertySource("application.properties")
public class TXRunner extends Thread{
    @Autowired
    private Environment env;
    private VinService VinService;

    public TXRunner(VinService vinService) {
        this.VinService = vinService;
    }

    public void run() {

        List<VINEntry> Vins = VinService.findAllUnknownAbove(500000);
        System.out.println("Unknown VINS: " + Vins.size());
        List<String> IPs = IPLISTS.TestIPs;
        List<String> TXCOOKIES = IPLISTS.TXCOOKIES;
        int numThreads = IPs.size();
        //Split list of all VINs into multiple lists. One list for each thread.
        ArrayList<List<VINEntry>> lists = ListSplitter.split(Vins, numThreads, 0);
        TXListener listener = new TXListener(numThreads);
        listener.start();
        for(int i=0; i <numThreads; i++ ){
            String cookie = "ASP.NET_SessionId=" + TXCOOKIES.get(i) + "; path=/; domain=.www.mytxcar.org; HttpOnly; Expires=Tue, 19 Jan 2038 03:14:07 GMT;";
            //initiate a new thread for each proxy IP.
            TXFetcher TXFetcher = new TXFetcher(VinService, lists.get(i),cookie,  IPs.get(i), listener, i);
            TXFetcher.start();
        }
    }


}
