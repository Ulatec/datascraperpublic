package DataScraper.Runner;


import DataScraper.Model.VINEntry;
import DataScraper.Service.VinService;
import DataScraper.ThreadListiner.NMVTISListener;
import DataScraper.Utils.IPLISTS;
import DataScraper.Utils.ListSplitter;

import java.util.ArrayList;
import java.util.List;

public class NMVTISRunner extends Thread{
        private VinService VinService;
        public NMVTISRunner(VinService vinService){
            this.VinService = vinService;
        }
        public void run(){
            List<VINEntry> Vins = VinService.findAllNotInNmvtisAndInUS();
            //get all IPs
            List<String> IPs = IPLISTS.AllIPs;

            int numThreads = IPs.size();
            //Split list of all VINs into multiple lists. One list for each thread.
            ArrayList<List<VINEntry>> lists = ListSplitter.split(Vins, numThreads, 0);

            NMVTISListener listener = new NMVTISListener(numThreads);
            for(int i=0; i <numThreads; i++ ){
                //initiate a new thread for each proxy IP.
                NMVTISProxy fetcher = new NMVTISProxy(VinService, lists.get(i), IPs.get(i),listener, i);
                fetcher.start();
            }
        }
}
