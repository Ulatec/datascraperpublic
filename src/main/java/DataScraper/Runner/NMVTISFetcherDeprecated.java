package DataScraper.Runner;

import DataScraper.Model.VINEntry;
import DataScraper.Service.VinService;
import DataScraper.Utils.Duplicates;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NMVTISFetcherDeprecated extends Thread{

    private VinService vinService;
    private List<VINEntry> vins;


    public NMVTISFetcherDeprecated(VinService vinService, List<VINEntry> vins){
        this.vinService = vinService;
        this.vins = vins;

    }
    public void run() {
        List<VINEntry> foundVins = new ArrayList<>();
        int counter = 0;
        double tested = 0;
        String VIN;
        Document doc;
        for (VINEntry vin : vins) {
            try {
                VIN = vin.getVin();
                doc = Jsoup.connect("https://secure.dmvdesk.com/webapi/NmvtisServiceJson.ashx?method=VerifyVinHasRecords&vin=" + VIN)
                        .get();
                tested = tested + 1;
                if (doc.body().ownText().equals("false")) {
                    System.out.println("[ " + new Date() + "] " + "[NMVTIS] " + vin.getVin() + " VIN not found. Found: " + counter + " Tested: " + tested + " %: " + (tested / vins.size()) * 100);
                } else if (doc.body().ownText().equals("true")) {
                    foundVins.add(vin);

                    System.out.println("VIN: " + vin.getVin());
                    System.out.println(doc.body().ownText());
                    for (VINEntry entry : foundVins) {
                        System.out.println(entry.getVin());
                    }
                    //Set stationId

                    //vin.setInspection(date);
                    vin.setNMVTISDate(new Date());
                    //Set Found
                    vin.setInNMVTIS(true);
                    //vin.setFound(true);
                    //Save to DB
                    vinService.save(vin);
                    Duplicates.test(vinService, vin);
                    counter = counter + 1;
                }
                sleep(300);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
