package DataScraper.Runner;

import DataScraper.Model.VINEntry;
import DataScraper.Service.VinService;
import DataScraper.ThreadListiner.TXListener;
import DataScraper.Utils.Duplicates;
import DataScraper.Utils.ListTrimmer;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

class TXFetcher extends Thread{

        private VinService vinService;
        private List<VINEntry> vins;
        private String cookie;
        private TXListener txListener;
        private int threadNum;
        private String IP;

    TXFetcher(VinService vinService, List<VINEntry> vins, String cookie, String IP, TXListener txListener, int threadNum){
        this.vinService = vinService;
        this.vins = vins;
        this.cookie = cookie;
        this.txListener = txListener;
        this.threadNum = threadNum;
        this.IP = IP;
    }

    public void run() {
        Instant start = Instant.now();
        List<VINEntry> foundVins = new ArrayList<>();
        int counter = 0;
        int tested = 0;
        String VIN;
        Elements elements;
        //Document doc;
        int results;
        List<VINEntry> trimmedList = ListTrimmer.random(vins, 25);
        Collections.shuffle(trimmedList);
        for (VINEntry vin : trimmedList) {
            try {
                VIN = vin.getVin();
                 Connection.Response res = Jsoup.connect("http://www.mytxcar.org/txcar_net/SearchVehicleTestHistory.aspx" +
                         "?__VIEWSTATE=%2FwEPDwUJMTYyNzMxMzU5D2QWAgIDD2QWBgIDDxYCHgdWaXNpYmxlaGQCBQ8WAh8AaGQCBw9kFgICAw8PFgIeBFRleHRlZGRkFN5xHAQ61AM9o6tJxuOorR%2B3gS8vBIZP2UJvtKowpkw%3D&__VIEWSTATEGENERATOR=959C499D&__EVENTVALIDATION=%2FwEdAAS0axPxTEgJzrk71wqEgCuYQy8oepNwkU1a7YXtygNIX2Byb67H6Xr9QQDvA%2ByrBQ8h4a%2FnMt9WzatTmqwjRS9e%2B4dmjunNj9OvWO%2BhNjvBPt7kpcBszI7%2B29Mhbc%2Fs1ik%3D&" +
                         "txtVin=&" + VIN +
                         "&hidAction=VIN&" +
                         "hidValue=" + VIN)
                         .proxy(IP, 3128)
                        .header("Cookie", cookie)
                         .method(Connection.Method.GET)
                        .execute();
                 if(res.statusCode() == 200) {
                     //System.out.println(res.bodyStream());
                     System.out.println(res.body());
                     elements = res.parse().getElementById("resultsDiv").children();
                     tested = tested + 1;
                     //Calculate time difference from start until now.
                     double remainingMinutes = ((trimmedList.size() - tested) / ((double) tested / (double) Duration.between(start, Instant.now()).toMillis()) * 1000) / 60;
                     if (elements.size() <= 2) {
                         //Log scraping progress.
                         System.out.println("[ " + new Date() + "] " + "[TX] [" + threadNum + "] " + vin.getVin() + " VIN not found. Found: " + counter + " Tested: " + tested + " %: " + (((double) tested / (double) trimmedList.size())) * 100 + " Remaining: " + (int) remainingMinutes);
                     } else {
                         foundVins.add(vin);
                         results = Integer.parseInt(elements.get(0).child(0).child(9).ownText().trim());
                         String station;
                         Element endTime;
                         station = elements.get(results + 2).child(0).child(1).child(2).ownText();
                         vin.setStation(elements.get(results + 2).child(0).child(1).child(2).ownText());
                         endTime = elements.get(results + 2).child(0).child(1).child(1);
                         SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
                         Date date = format.parse(endTime.ownText());
                         for (VINEntry entry : foundVins) {
                             System.out.println(entry.getVin());
                         }
                         List<VINEntry> pulledEntries = vinService.findByVin(vin.getVin());
                         if (pulledEntries.size() == 1) {
                             VINEntry pulledEntry = pulledEntries.get(0);
                             //Update Database entry.
                             pulledEntry.setInspection(date);
                             pulledEntry.setDateFound(new Date());
                             pulledEntry.setState("TX");
                             pulledEntry.setStation(station);

                             pulledEntry.setFound(true);
                             vinService.save(pulledEntry);
                             //Check database for identical VIN serials
                             Duplicates.test(vinService, pulledEntry);
                         } else {
                             System.out.println("not a single record. Multiple records found.");
                         }
                         counter = counter + 1;
                     }
                 }
            }catch(Exception e){
                if(e instanceof NullPointerException){
                    System.out.println("[" + threadNum +"] I HAVE A NULLPOINTER. LIKELY CAPTCHA.");

                    boolean captchaPresent = false;
                    try {
                        captchaPresent = checkForCaptch();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if(captchaPresent){
                        //To be implemented
                    }
                }else{
                    System.out.println("[" + threadNum +"] has an exception. ");
                }
                e.printStackTrace();
            }
            try {
                int rand = (int) (Math.random()*3150);
                sleep(6775 + rand);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        txListener.threadFinish(threadNum);
    }
    private boolean checkForCaptch() throws IOException {
        Document doc = Jsoup.connect("http://www.mytxcar.org/txcar_net/SearchVehicleTestHistory.aspx")
                .proxy(IP, 3128)
                .header("Cookie", cookie)
                .get();
        return doc.body().getElementById("captcha") == null;
    }
}
