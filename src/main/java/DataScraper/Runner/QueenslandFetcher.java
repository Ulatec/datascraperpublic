package DataScraper.Runner;

import DataScraper.Model.VINEntry;
import DataScraper.Service.VinService;
import DataScraper.Utils.Duplicates;
import DataScraper.Utils.ListTrimmer;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class QueenslandFetcher extends Thread{
    private List<VINEntry> vins;
    private VinService vinService;
    private String IP;
    private int thread;

    public QueenslandFetcher(VinService vinService, List<VINEntry> vins, String IP, int thread){
        this.vinService = vinService;
        this.vins = vins;
        this.IP = IP;
        this.thread = thread;
    }


    public void run() {
        System.out.println(vins.size());
        Instant start = Instant.now();
        int counter = 0;
        int tested = 0;
        List<VINEntry> trimmedList = ListTrimmer.random(vins, 28);
        Collections.shuffle(trimmedList);
        for (VINEntry vin : trimmedList) {
            try {
                tested++;
                String VIN = vin.getVin();
                String AussieVIN = VIN.replace("5YJ3E1E", "5YJ3F7E");
                //First request to get cookies
                Connection.Response res0 = Jsoup.connect("https://www.service.transport.qld.gov.au/checkrego/application/TermAndConditions.xhtml?dswid=-836")
                        .proxy(IP, 3128)
                        .method(Connection.Method.GET)
                        .execute();
                Document doc = res0.parse();
                //Accept terms of use.
                Connection.Response res1 = Jsoup.connect("https://www.service.transport.qld.gov.au/checkrego/application/TermAndConditions.xhtml?dswid=-836")
                        .proxy(IP, 3128)
                        .method(Connection.Method.POST)
                        .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36 OPR/63.0.3368.55666")
                        .header("Upgrade-Insecure-Requests", "1")
                        .header("Sec-Fetch-User", "?1")
                        .header("Sec-Fetch-Site", "same-origin")
                        .header("Sec-Fetch-Mode", "navigate")
                        .header("Content-Type", "application/x-www-form-urlencoded")
                        .header("Host", "www.service.transport.qld.gov.au")
                        .header("Origin", "https://www.service.transport.qld.gov.au")
                        .header("Referer", "https://www.service.transport.qld.gov.au/checkrego/application/TermAndConditions.xhtml?dswid=-836")
                        .header("Content-Type", "application/x-www-form-urlencoded")
                        .cookie("JSESSIONID", res0.cookies().get("JSESSIONID"))
                        .data("tAndCForm:confirmButton", "")
                        .data("tAndCForm_SUBMIT", "1")
                        .data("javax.faces.ClientWindow", "-836")
                        .data("javax.faces.ViewState", doc.getElementById("j_id__v_0:javax.faces.ViewState:1").val())
                        .followRedirects(true)
                        .execute();
                //Finally test if VIN is found in their database.
                Document doc1 = res1.parse();
                Connection.Response res = Jsoup.connect("https://www.service.transport.qld.gov.au/checkrego/application/VehicleSearch.xhtml?dswid=-836")
                        .proxy(IP, 3128)
                        .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36 OPR/63.0.3368.55666")
                        .header("Upgrade-Insecure-Requests", "1")
                        .header("Sec-Fetch-User", "?1")
                        .header("Sec-Fetch-Site", "same-origin")
                        .header("Sec-Fetch-Mode", "navigate")
                        .header("Content-Type", "application/x-www-form-urlencoded")
                        .header("Host", "www.service.transport.qld.gov.au")
                        .header("Origin", "https://www.service.transport.qld.gov.au")
                        .header("Referer", "https://www.service.transport.qld.gov.au/checkrego/application/VehicleSearch.xhtml?dswid=-836")
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
                        .header("DNT", "1")
                        .header("Content-Length", "247")
                        .header("Accept-Encoding", "gzip, deflate, br")
                        .header("Accept-Language", "en-US,en;q=0.9")
                        .header("Connection", "keep-alive")
                        .header("Pragma", "no-cache")
                        .cookie("JSESSIONID", res0.cookies().get("JSESSIONID"))
                        .data("vehicleSearchForm:plateNumber", "")
                        .data("vehicleSearchForm:referenceId", AussieVIN)
                        .data("vehicleSearchForm:confirmButton", "")
                        .data("javax.faces.ClientWindow", "-836")
                        .data("vehicleSearchForm_SUBMIT", "1")
                        .data("javax.faces.ViewState", doc1.getElementById("j_id__v_0:javax.faces.ViewState:1").val())
                        .followRedirects(true)
                        .method(Connection.Method.POST)
                        .execute();

                Elements data = res.parse().getElementsByClass("data");
                //Calculate time difference from start until now.
                double remainingMinutes = ((trimmedList.size() - tested)/((double)tested/(double)Duration.between(start, Instant.now()).toMillis())*1000)/60;
                if (data.size() > 0) {
                    List<VINEntry> pulledEntries = vinService.findByVin(vin.getVin());
                    if(pulledEntries.size() == 1) {
                        counter++;
                        VINEntry pulledEntry = pulledEntries.get(0);
                        Element reg = data.get(1).child(3);
                        Date tempDate = new SimpleDateFormat("dd/MM/yyyy").parse(reg.ownText());
                        Calendar regCal = Calendar.getInstance();
                        regCal.setTime(tempDate);
                        regCal.set(Calendar.YEAR, regCal.get(Calendar.YEAR) - 1);
                        Date newDate = regCal.getTime();
                        //Update Database entry.
                        pulledEntry.setDateFound(new Date());
                        pulledEntry.setNonUS(true);
                        pulledEntry.setFound(true);
                        pulledEntry.setRegDate(newDate);
                        pulledEntry.setVin(AussieVIN);
                        vinService.save(pulledEntry);
                        //Check database for identical VIN serials
                        Duplicates.test(vinService, pulledEntry);
                        System.out.println("[ " + new Date() + "] " + "[Queensland] " + "[P" + thread + "] " + AussieVIN + " VIN found. Found: " + counter + " Tested: " + tested + " %: " + ((double) tested / (double) trimmedList.size()) * 100 + " Remaining: " + (int)remainingMinutes) ;
                    }else{

                        System.out.println("not a single record. Multiple records found.");
                    }

                } else {
                    System.out.println("[ " + new Date() + "] " + "[Queensland] " + "[P" + thread + "] " + AussieVIN + " VIN not found. Found: " + counter + " Tested: " + tested + " %: " + ((double) tested / (double) trimmedList.size()) * 100 + " Remaining: " + (int)remainingMinutes);
                }
                int rand = (int) (Math.random()*1900);
                sleep(5375 + rand);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(thread + " has a problem. IP: " + IP);
            }
        }
    }
}
