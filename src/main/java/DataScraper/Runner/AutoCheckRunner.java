package DataScraper.Runner;

import DataScraper.Model.VINEntry;
import DataScraper.Service.VinService;
import DataScraper.ThreadListiner.AutocheckListener;
import DataScraper.Utils.IPLISTS;
import DataScraper.Utils.ListSplitter;

import java.util.ArrayList;
import java.util.List;

public class AutoCheckRunner extends Thread{

    private VinService vinService;

    public AutoCheckRunner(VinService vinService) {
        this.vinService = vinService;
    }

    public void run() {

        List<String> IPs = IPLISTS.AllIPs;


        int numThreads = IPs.size();
        List<VINEntry> Vins = vinService.findAllNotInAutoCheckAndInUS();
        //Split list of all VINs into multiple lists. One list for each thread.
        ArrayList<List<VINEntry>> lists = ListSplitter.split(Vins, numThreads, 0);
        AutocheckListener listener = new AutocheckListener(numThreads);
        for(int i=0; i <numThreads; i++ ){
            //initiate a new thread for each proxy IP.
            AutoCheckFetcher fetcher = new AutoCheckFetcher(vinService, lists.get(i), IPs.get(i),listener, i);
            fetcher.start();
        }
    }
}
