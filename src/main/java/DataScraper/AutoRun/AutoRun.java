package DataScraper.AutoRun;

import DataScraper.GUI.Menu;
import DataScraper.Repository.VinRepository;
import DataScraper.Service.VinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

@Component
//Run AutoRun check before Menu. If no valid Autorun flag found, go to GUI.Menu.
@Order(2)
@PropertySource("application.properties")
public class AutoRun implements ApplicationRunner {
    @Autowired
    private Environment env;
    @Autowired
    private VinService vinService;
    @Autowired
    private VinRepository repo;
    @Override
    public void run(ApplicationArguments args) throws Exception {
            String tempString = env.getProperty("scraper.task");
            System.out.println(tempString);
            Timer timer = new Timer();
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 30);
        today.set(Calendar.SECOND, 0);
            if (tempString != null && !tempString.isEmpty()) {
                AutoRunTask autoRunTask = new AutoRunTask(vinService, tempString);
                //Run autorun task every day.
                timer.schedule(autoRunTask, today.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));
            } else {
                System.out.println("No autorun.");
                System.out.println("Opening Menu.");
                Menu menu = new Menu(vinService, repo);
                menu.run();
            }

    }
}
