package DataScraper.AutoRun;

import DataScraper.Runner.*;
import DataScraper.Service.VinService;
import DataScraper.Runner.QueenslandRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.util.TimerTask;
@PropertySource("application.properties")
public class AutoRunTask extends TimerTask {
    @Autowired
    private Environment env;

    private VinService vinService;
    private String task;
    AutoRunTask(VinService vinService, String task ){
        this.vinService = vinService;
        this.task = task;
    }
    @Override
    public void run() {
        //Surely, there is a better what to write this. Switch for now.
        //TODO: Autorun without switch case.
        switch (task) {
            case "NMVTIS":
                System.out.println("Starting NMVTIS");
                NMVTISRunner nmvtisRunner = new NMVTISRunner(vinService);
                nmvtisRunner.start();
                break;
            case "OH":
                System.out.println("Starting OHIO");
                OHRunner ohrunner = new OHRunner(vinService);
                ohrunner.start();
                break;
            case "TX":
                System.out.println("Starting TEXAS");
                TXRunner TXRunner = new TXRunner(vinService);
                TXRunner.start();
                break;
            case "CARFAX":
                System.out.println("Starting CARFAX");
                CarfaxRunner ctest = new CarfaxRunner(vinService);
                ctest.start();
                break;
            case "MYCARFAX":
                System.out.println("Starting MYCARFAX");
                myCarfax cfx = new myCarfax(vinService);
                cfx.start();
                break;
            case "AUSTRALIA":
                System.out.println("Starting AUSTRALIA");
                QueenslandRunner queenslandRunner = new QueenslandRunner(vinService);
                queenslandRunner.start();
                break;
            case "AUTOCHECK":
                System.out.println("Starting AUTOCHECK");
                AutoCheckRunner autoCheckRunner = new AutoCheckRunner(vinService);
                autoCheckRunner.start();
                break;
            case "CARJAM":
                System.out.println("Starting CARJAM");
                CarJamRunner carJamRunner = new CarJamRunner(vinService);
                carJamRunner.start();
                break;
        }
    }
}
