package DataScraper.Test;

import DataScraper.Model.VINEntry;
import DataScraper.Service.VinService;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;

public class AutoCheckAccidentsThread extends Thread{
    private VinService vinService;
    private int threadNum;
    private List<VINEntry> vins;
    private String IP;
    public AutoCheckAccidentsThread(VinService vinService, List<VINEntry> vins, String IP, int threadNum) {
        this.vinService = vinService;
        this.threadNum = threadNum;
        this.vins = vins;
        this.IP = IP;
    }

    public void run(){
        Instant start = Instant.now();
        int counter = 0;
        int tested = 0;
        for(VINEntry vin : vins){
            try {

                Document doc = Jsoup.connect("https://www.autocheck.com/consumer-api/meta/v1/summary/" + vin.getVin())
                        .proxy(IP, 3128)
                        .ignoreContentType(true)
                        .get();
                tested++;
                JSONObject json = new JSONObject(doc.body().ownText().replace("[", "").replace("]", ""));


                List<VINEntry> pulledEntries = vinService.findByVin(vin.getVin());
                if(pulledEntries.size() == 1) {
                    VINEntry pulledEntry = pulledEntries.get(0);
                    //Update Database entry.
                    pulledEntry.setAccident((boolean)json.get("accidentRecord"));
                    vinService.save(pulledEntry);

                }else {
                    System.out.println("not a single record. Multiple records found.");
                }

                double remainingMinutes = ((vins.size() - tested)/((double)tested/(double)Duration.between(start, Instant.now()).toMillis())*1000)/60;
                if((boolean)json.get("accidentRecord")){
                    counter++;
                }
                System.out.println("[ " + new Date() + "] " + "[AutoCheck Accidents] " + "[P" + threadNum +"] " + vin.getVin() + " VIN not found. Found: " + counter + " Tested: " + tested + " %: " + ((double)tested / (double)vins.size()) * 100 + " Remaining: " + (int)remainingMinutes);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Thread #" + threadNum + " Has a problem. IP Address: " + IP );
            }
            try {
                int rand = (int) (Math.random()*725);
                sleep(1750 + rand);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
