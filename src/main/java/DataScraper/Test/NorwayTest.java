package DataScraper.Test;

import DataScraper.Model.VINEntry;
import DataScraper.Utils.Duplicates;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.DecimalFormat;
import java.time.temporal.ChronoUnit;
import java.util.*;
//TODO: needs cleaning and comments
public class NorwayTest extends Thread{
    private DataScraper.Service.VinService VinService;


    public NorwayTest(DataScraper.Service.VinService vinService) {
        this.VinService = vinService;
    }

    public void run() {
        System.out.println("I AM STARTING");
        int counter = 0;
        double tested = 0;

        try {
            Date init = new Date();
            Calendar today = Calendar.getInstance();
            today.setTime(init);
            Calendar janFirst = Calendar.getInstance();
            janFirst.set(2019,Calendar.JANUARY,1);
            long daysBetween = ChronoUnit.DAYS.between(janFirst.toInstant(), today.toInstant());
            System.out.println("days: " + daysBetween);
            Calendar movingDate = Calendar.getInstance();
            movingDate.setTime(init);
            DecimalFormat mFormat= new DecimalFormat("00");
            for(int i = 0; i<= daysBetween; i++){
                String dateString = mFormat.format(Double.valueOf(movingDate.get(Calendar.DATE))) + "." + mFormat.format(Double.valueOf(movingDate.get(Calendar.MONTH) + 1)) + "." + movingDate.get(Calendar.YEAR);
                System.out.println(dateString);
                movingDate.add(Calendar.DATE, -1);
                System.out.println(dateString);

                Document doc = Jsoup.connect("https://teslastats.no/tesla-list.php?date=" + dateString + "&size=1920&lang=US")
                        .get();
                Elements entries = doc.body().getElementsByClass("tbl");
                for(Element element : entries){
                    Elements elements = element.getElementsByClass("tdl");
                    if (elements.get(2).ownText().equals("Model 3")){
                        String trim = elements.get(6).ownText();
                        if(trim.equals("LR")){
                            String vinNumber = elements.get(4).ownText();
                            System.out.println(vinNumber);
                            List<VINEntry> vins = VinService.findByVinNumber(Integer.parseInt(vinNumber));
                            for(VINEntry vin : vins){
                                System.out.println(vin.getVin());
                                if(Integer.parseInt(elements.get(4).ownText()) > 372000 && Integer.parseInt(elements.get(4).ownText()) < 376000) {
                                    if (vin.getVin().contains("A")) {
                                        vin.setFound(true);
                                        vin.setNonUS(true);
                                        vin.setRegDate(movingDate.getTime());
                                        vin.setCity(elements.get(5).ownText());
                                        vin.setTrim(elements.get(6).ownText());
                                        vin.setDateFound(new Date());
                                        VinService.save(vin);
                                        Duplicates.test(VinService, vin);
                                    } else {
                                        System.out.println(vin.getVin() + " was not a match.");
                                    }
                                } else if(Integer.parseInt(elements.get(4).ownText()) > 340000 && Integer.parseInt(elements.get(4).ownText()) < 341000) {
                                    if (vin.getVin().contains("A")) {
                                        vin.setFound(true);
                                        vin.setNonUS(true);
                                        vin.setRegDate(movingDate.getTime());
                                        vin.setCity(elements.get(5).ownText());
                                        vin.setTrim(elements.get(6).ownText());
                                        vin.setDateFound(new Date());
                                        VinService.save(vin);
                                        Duplicates.test(VinService, vin);
                                    } else {
                                        System.out.println(vin.getVin() + " was not a match.");
                                    }
                                }
                                else if(Integer.parseInt(elements.get(4).ownText()) > 343500 && Integer.parseInt(elements.get(4).ownText()) < 344900) {
                                    if (vin.getVin().contains("A")) {
                                        vin.setFound(true);
                                        vin.setNonUS(true);
                                        vin.setRegDate(movingDate.getTime());
                                        vin.setCity(elements.get(5).ownText());
                                        vin.setTrim(elements.get(6).ownText());
                                        vin.setDateFound(new Date());
                                        VinService.save(vin);
                                        Duplicates.test(VinService, vin);
                                    } else {
                                        System.out.println(vin.getVin() + " was not a match.");
                                    }
                                }
                                    else if(Integer.parseInt(elements.get(4).ownText()) > 318500 && Integer.parseInt(elements.get(4).ownText()) < 319000) {
                                    if (vin.getVin().contains("A")) {
                                        vin.setFound(true);
                                        vin.setNonUS(true);
                                        vin.setRegDate(movingDate.getTime());
                                        vin.setCity(elements.get(5).ownText());
                                        vin.setTrim(elements.get(6).ownText());
                                        vin.setDateFound(new Date());
                                        VinService.save(vin);
                                        Duplicates.test(VinService, vin);
                                    } else {
                                        System.out.println(vin.getVin() + " was not a match.");
                                    }
                                }
                                        else if(Integer.parseInt(elements.get(4).ownText()) > 340000 && Integer.parseInt(elements.get(4).ownText()) < 341000) {
                                    if (vin.getVin().contains("A")) {
                                        vin.setFound(true);
                                        vin.setNonUS(true);
                                        vin.setRegDate(movingDate.getTime());
                                        vin.setCity(elements.get(5).ownText());
                                        vin.setTrim(elements.get(6).ownText());
                                        vin.setDateFound(new Date());
                                        VinService.save(vin);
                                        Duplicates.test(VinService, vin);
                                    } else {
                                        System.out.println(vin.getVin() + " was not a match.");
                                    }
                                }
                                 else if(Integer.parseInt(elements.get(4).ownText()) > 349000 && Integer.parseInt(elements.get(4).ownText()) < 351000) {
                                        if (vin.getVin().contains("A")) {
                                            vin.setFound(true);
                                            vin.setNonUS(true);
                                            vin.setRegDate(movingDate.getTime());
                                            vin.setCity(elements.get(5).ownText());
                                            vin.setTrim(elements.get(6).ownText());
                                            vin.setDateFound(new Date());
                                            VinService.save(vin);
                                            Duplicates.test(VinService, vin);
                                        } else {
                                            System.out.println(vin.getVin() + " was not a match.");
                                        }
                                }else if(Integer.parseInt(elements.get(4).ownText()) < 267000){
                                    if (vin.getVin().contains("B")) {
                                        vin.setFound(true);
                                        vin.setNonUS(true);
                                        vin.setRegDate(movingDate.getTime());
                                        vin.setCity(elements.get(5).ownText());
                                        vin.setTrim(elements.get(6).ownText());
                                        vin.setDateFound(new Date());
                                        VinService.save(vin);
                                        Duplicates.test(VinService, vin);
                                    }else{
                                        System.out.println(vin.getVin() + " was not a match.");
                                    }
                                }else{
                                    if (vin.getVin().contains("B")) {
                                        vin.setFound(true);
                                        vin.setNonUS(true);
                                        vin.setRegDate(movingDate.getTime());
                                        vin.setCity(elements.get(5).ownText());
                                        vin.setTrim(elements.get(6).ownText());
                                        vin.setDateFound(new Date());
                                        VinService.save(vin);
                                        Duplicates.test(VinService, vin);
                                    }else{
                                        System.out.println(vin.getVin() + " was not a match.");
                                    }
                                }
                            }
                        }
                        if(trim.equals("SR+")){
                            String vinNumber = elements.get(4).ownText();
                            System.out.println(vinNumber);
                            List<VINEntry> vins = VinService.findByVinNumber(Integer.parseInt(vinNumber));
                            for(VINEntry vin : vins){
                                System.out.println(vin.getVin());
                                if(vin.getVin().contains("A")){
                                    vin.setFound(true);
                                    vin.setNonUS(true);
                                    vin.setRegDate(movingDate.getTime());
                                    vin.setCity(elements.get(5).ownText());
                                    vin.setTrim(elements.get(6).ownText());
                                    vin.setDateFound(new Date());
                                    VinService.save(vin);
                                    Duplicates.test(VinService, vin);
                                }else{
                                    System.out.println(vin.getVin() + " was not a match.");
                                }
                            }
                        }
                        if(trim.equals("P")){
                            String vinNumber = elements.get(4).ownText();
                            System.out.println(vinNumber);
                            List<VINEntry> vins = VinService.findByVinNumber(Integer.parseInt(vinNumber));
                            for(VINEntry vin : vins){
                                System.out.println(vin.getVin());
                                if(vin.getVin().contains("B")){
                                    vin.setFound(true);
                                    vin.setNonUS(true);
                                    vin.setRegDate(movingDate.getTime());
                                    vin.setCity(elements.get(5).ownText());
                                    vin.setTrim(elements.get(6).ownText());
                                    vin.setDateFound(new Date());
                                    VinService.save(vin);
                                    Duplicates.test(VinService, vin);
                                }else{
                                    System.out.println(vin.getVin() + " was not a match.");
                                }
                            }
                        }


                        System.out.println(elements.get(5).ownText());
                        System.out.println(elements.get(6).ownText());
                    }
                }
            }


            String ACCOUNT_SID = "ACd36e4fae72d52ebcd8bc5593952fb6d2";
            String AUTH_TOKEN = "f28cac58baa3e7f177cd7178aa0f45d2";
            Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
            Message smsMessage = Message.creator(
                    new com.twilio.type.PhoneNumber("+17608924462"),
                    new com.twilio.type.PhoneNumber("+14697131789"),
                    "Norway completed.").create();
            System.out.println(smsMessage.getSid());


        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
