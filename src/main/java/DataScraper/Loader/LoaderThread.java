package DataScraper.Loader;

import DataScraper.Model.VINEntry;
import DataScraper.Repository.VinRepository;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
//TODO: Use  loaderThread to speed up DataLoader's single threaded times.
public class LoaderThread  extends Thread{
    private VinRepository vinRepository;
    private List<VINEntry> vins;
    private List<String[]> sheetVins;
    LoaderThread(VinRepository vinRepository, List<VINEntry> vins, List<String[]> sheetVins){
        this.vinRepository = vinRepository;
        this.vins = vins;
        this.sheetVins = sheetVins;

    }

    @Override
    public void run() {
        Instant start = Instant.now();
        int counter = 0;
        int total = sheetVins.size();
        for(String[] VINS : sheetVins){
            for(String VIN : VINS){
                System.out.println(VIN);
                System.out.println(VIN.substring(11,17));
                VINEntry tempVin = new VINEntry(VIN);
                tempVin.setNonUS(false);
                tempVin.setVinNumber(Integer.parseInt(VIN.substring(11,17)));
                Instant now = Instant.now();
                long delta = Duration.between(start, now).toMillis();
                double rate= ((double)counter/(double)delta)*1000;
                int remaining = vins.size() - counter;
                double remainingMinutes = (remaining/rate)/60;
                if(!checkIfVinInDatabase(tempVin, vins)){
                    vinRepository.save(tempVin);
                    System.out.println(tempVin.getVin() + " was not found in DB" + (((double)counter/(double)total)*100) + "%"  + " Remaining: " + (int)remainingMinutes);
                }else{
                    System.out.println(tempVin.getVin() + " was found in DB" + (((double)counter/(double)total)*100) + "%"  + " Remaining: " + (int)remainingMinutes);
                }
                counter++;
            }

        }
    }
    private boolean checkIfVinInDatabase(VINEntry vin, List<VINEntry> VinsInDatabase){
        for(VINEntry entry : VinsInDatabase){
            if(vin.getVin().equals( entry.getVin())){
                return true;
            }
        }
        return false;
    }
}
