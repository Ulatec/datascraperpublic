package DataScraper.Loader;


import DataScraper.Model.VINEntry;
import DataScraper.Repository.VinRepository;
import com.opencsv.CSVReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Component

public class DataLoader extends Thread{
    @Autowired
    private final VinRepository repo;

    private int counter;
    private int total;
    @Autowired
    public DataLoader(VinRepository repo) {
        this.repo = repo;
    }


    public void run(){
        counter = 0;
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader("C:/Users/Mark/Desktop/Java Projects/DataScraper/src/main/resources/Vins.csv"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        List<String[]> myEntries = null;
        try {
            assert reader != null;
            myEntries = reader.readAll();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<VINEntry> list = (List<VINEntry>) repo.findAll();
        assert myEntries != null;
        total = myEntries.size();
        for(String[] VINS : myEntries){
            for(String VIN : VINS){
                System.out.println(VIN);
                System.out.println(VIN.substring(11,17));
                VINEntry tempVin = new VINEntry(VIN);
                tempVin.setNonUS(false);
                try {
                    tempVin.setVinNumber(Integer.parseInt(VIN.substring(11, 17)));
                }catch(Exception e){
                    e.printStackTrace();
                }
                if(!checkIfVinInDatabase(tempVin, list)){
                    repo.save(tempVin);
                    System.out.println(tempVin.getVin() + " was not found in DB" + (((double)counter/(double)total)*100) + "%");
                }else{
                    System.out.println(tempVin.getVin() + " was found in DB" + (((double)counter/(double)total)*100) + "%");
                }
                counter++;
            }
            //TODO: Implement LoaderThread to improve csv import times.
        }

        //System.out.println(list.size());
    }

    private boolean checkIfVinInDatabase(VINEntry vin, List<VINEntry> VinsInDatabase){
        for(VINEntry entry : VinsInDatabase){
            if(vin.getVin().equals( entry.getVin())){
                return true;
            }
        }
        return false;
    }
    public static ArrayList<List<String[]>> split(List<String[]> fulllist, int numOfOutputs) {
        // get size of the list
        List<String[]> list = fulllist.subList(0,fulllist.size());
        int size = list.size();
        ArrayList<List<String[]>> lists = new ArrayList<>();
        int lastDivider = 0;
        for(int i = 0;i < numOfOutputs; i++){
            int newDivider = (int) Math.round(size*((i+1)/(double)numOfOutputs));
            lists.add(new ArrayList<>(list.subList(lastDivider, newDivider)));
            lastDivider = newDivider + 1;
        }
        // return an List array to accommodate both lists
        return lists;
    }
}
