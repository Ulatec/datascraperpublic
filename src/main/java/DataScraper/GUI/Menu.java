package DataScraper.GUI;
import DataScraper.CSV.NMVTIS;
import DataScraper.CSV.NYCSV;
import DataScraper.Cleaner.RemoveDuplicates;
import DataScraper.Loader.DataLoader;
import DataScraper.Repository.VinRepository;
import DataScraper.Runner.*;
import DataScraper.Service.VinService;
import DataScraper.Test.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

//@Component
//@Order(3)
public class Menu{

    private VinRepository repo;


    private VinService vinService;

    private BufferedReader mReader;
    private HashMap<String, String> mMenu;


    public Menu (VinService vinService, VinRepository repo) {
        mReader = new BufferedReader(new InputStreamReader(System.in));
        mMenu = new LinkedHashMap<>();
        this.vinService = vinService;
        this.repo = repo;
        buildMenu();
    }

    private void buildMenu(){
        //LinkedHashMap to maintain menu order.
        mMenu.put("1","get TX");
        mMenu.put("2","get OH");
        mMenu.put("3", "dataloader");
        mMenu.put("4","NY CSV import");
        mMenu.put("5","NMVTIS CSV import");
        mMenu.put("6", "Remove Duplicates");
        mMenu.put("7", "Carfax");
        mMenu.put("8", "myCarfax");
        mMenu.put("9", "NMVTIS");
        mMenu.put("10", "NorwayTest");
        mMenu.put("11", "AutoCheckTest");
        mMenu.put("12", "AussieTest");
        mMenu.put("13", "AutoCheckRunner");
        mMenu.put("14", "CarJamRunner");
        mMenu.put("quit","Exit the application.");
    }

    public void run(){
        String choice ="";

        do{
            try{
                choice = promptAction();
                switch (choice) {
                    case "1":
                        TXRunner TXRunner = new TXRunner(vinService);
                        TXRunner.start();
                        break;
                    case "2":
                        OHRunner ohrunner = new OHRunner(vinService);
                        ohrunner.start();
                        break;
                    case "3":
                        DataLoader load = new DataLoader(repo);
                        load.start();
                        break;
                    case "4":
                        NYCSV ny = new NYCSV(repo);
                        ny.run();
                        break;
                    case "5":
                        NMVTIS nmvtis = new NMVTIS(repo);
                        nmvtis.run();
                        break;
                    case "6":
                        RemoveDuplicates cleaner = new RemoveDuplicates(vinService, repo);
                        cleaner.start();
                        break;
                    case "7":
                        CarfaxRunner ctest = new CarfaxRunner(vinService);
                        ctest.start();
                    case "8":
                        myCarfax cfx = new myCarfax(vinService);
                        cfx.start();
                        break;
                    case "9":
                        NMVTISRunner nmvtisRunner = new NMVTISRunner(vinService);
                        nmvtisRunner.start();
                        break;
                    case "10":
                        NorwayTest NT = new NorwayTest(vinService);
                        NT.start();
                        break;
                    case "11":
                        AutoCheckAccidents ACT = new AutoCheckAccidents(vinService);
                        ACT.start();
                        break;
                    case "12":
                        QueenslandRunner Aussie = new QueenslandRunner(vinService);
                        Aussie.start();
                        break;
                    case "13":
                        AutoCheckRunner autoCheckRunner = new AutoCheckRunner(vinService);
                        autoCheckRunner.start();
                        break;
                    case "14":
                        CarJamRunner NZTest = new CarJamRunner(vinService);
                        NZTest.start();
                        break;
                    case "quit":
                        break;
                }
            }catch(IOException | NumberFormatException | IndexOutOfBoundsException exception){
                if(exception instanceof IndexOutOfBoundsException){
                    System.out.printf("Your input appears to not match any of the available options. %n");
                }else if(exception instanceof NumberFormatException){
                    System.out.printf("Please use only numerical digits when making selections. %n");
                }else {
                    System.out.println("Error parsing input! Let's try again! %n");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }while(!choice.equals("quit"));
    }

    private String promptAction()throws IOException{
        System.out.printf("%n Main Menu: %n");
        for(Map.Entry<String,String> option : mMenu.entrySet()){
            System.out.printf("%s - %s %n", option.getKey(), option.getValue());
        }
        System.out.println("What do you want to do: ");
        String choice = mReader.readLine();
        return choice.trim().toLowerCase();
    }

}



