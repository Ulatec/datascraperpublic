package DataScraper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataScraper {
    public static void main(String[] args) {
        SpringApplication.run(DataScraper.class, args);
    }
}
