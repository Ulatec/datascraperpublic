package DataScraper.Cleaner;

import DataScraper.Model.VINEntry;

import DataScraper.Service.VinService;
import DataScraper.Utils.Duplicates;

import java.util.ArrayList;
import java.util.List;

public class DuplicatesThread extends Thread {
    private List<VINEntry> vins;
    private VinService vinService;
    private int threadNum;
    DuplicatesThread(VinService vinService, List<VINEntry> vins, int threadNum){
        this.vins = vins;
        this.vinService = vinService;
        this.threadNum = threadNum;
    }

    public void run(){
        System.out.println("Starting Thread");
        ArrayList<VINEntry> vinsToDelete = new ArrayList<>();
        int counter = 0;
        int k = 0;
            for(VINEntry scannedVin : vins){
                Duplicates.test(vinService, scannedVin);
                counter++;
                System.out.println( scannedVin.getVin() + " " + counter);
            }

        System.out.println("duplicates: " + k);
        System.out.println(vinsToDelete);
    }


}
