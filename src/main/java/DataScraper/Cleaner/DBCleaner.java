package DataScraper.Cleaner;

import DataScraper.Model.VINEntry;
import DataScraper.Repository.VinRepository;
import DataScraper.Service.VinService;
import DataScraper.Utils.Duplicates;
import java.util.Date;
import java.util.List;

public class DBCleaner extends Thread {
    VinService vinService;
    VinRepository vinRepo;
    public DBCleaner(VinService vinService, VinRepository vinRepo) {
        this.vinService = vinService;
        this.vinRepo = vinRepo;
    }
    public void run(){
        List<VINEntry> allVins = vinRepo.findByFoundTrue();

        for(VINEntry entry : allVins){
            System.out.println("testing " + entry.getVin());
            Duplicates.test(vinService, entry);
        }


    }
}
