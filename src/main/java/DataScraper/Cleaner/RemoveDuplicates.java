package DataScraper.Cleaner;

import DataScraper.Model.VINEntry;
import DataScraper.Repository.VinRepository;

import DataScraper.Service.VinService;
import DataScraper.Utils.ListSplitter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RemoveDuplicates extends Thread {

    VinService vinService;
    VinRepository vinRepo;

    public RemoveDuplicates(VinService vinService, VinRepository vinRepo) {
        this.vinService = vinService;
        this.vinRepo = vinRepo;
    }

    public void run(){
        Date now = new Date();
        List<VINEntry> allVins = vinRepo.findByInNmvtisTrueOrCarfaxFoundTrueOrFoundTrue();
        Date finish = new Date();
        long totaltime = finish.getTime() - now.getTime();
        System.out.println(totaltime + " ms");

        ArrayList<List<VINEntry>> lists = ListSplitter.split(allVins, 16,0);
        int numThreads= 16;
        for(int i=0; i <numThreads; i++ ){
            DuplicatesThread thread = new DuplicatesThread(vinService, lists.get(i), i);
            thread.start();
        }


    }

}
