package DataScraper.CSV;

import DataScraper.Model.VINEntry;
import DataScraper.Repository.VinRepository;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import org.springframework.beans.factory.annotation.Autowired;

public class NYCSV {
    @Autowired
    private final VinRepository repo;


    @Autowired
    public NYCSV(VinRepository repo) {
        this.repo = repo;
    }

    public void run()
    {
        List<VINEntry> list = (List<VINEntry>) repo.findAll();

        try (
                Reader reader = Files.newBufferedReader(Paths.get("C:/Users/Mark/Desktop/Java Projects/DataScraper/src/main/resources/NYVINS.csv"))
        ) {

            CsvToBean<VINEntry> csvToBean = new CsvToBeanBuilder(reader)
                    .withType(VINEntry.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            Iterator<VINEntry> myUserIterator = csvToBean.iterator();
            int i = 0;
            while (myUserIterator.hasNext()) {

                VINEntry entry = myUserIterator.next();
                System.out.println("VIN : " + entry.getVin());
                System.out.println("VIN : " + entry.getRegDate());
                System.out.println("---------------------------" + i);
                if(checkIfVinInDatabase(entry, list)){
                    List<VINEntry> dbEntries = repo.findByVin(entry.getVin());
                    if(dbEntries.size() == 1) {
                        VINEntry dbEntry = dbEntries.get(0);
                        dbEntry.setRegDate(entry.getRegDate());
                        dbEntry.setFound(true);
                        dbEntry.setState("NY");
                        repo.save(dbEntry);
                    }
                }
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private boolean checkIfVinInDatabase(VINEntry vin, List<VINEntry> VinsInDatabase){
        for(VINEntry entry : VinsInDatabase){
            if(vin.getVin().equals( entry.getVin())){
                System.out.println(vin.getVin() + " was found");
                return true;
            }
        }
        System.out.println(vin.getVin() + " was not found");
        return false;
    }
}
