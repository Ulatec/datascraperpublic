package DataScraper.CSV;

import DataScraper.Model.VINEntry;
import DataScraper.Repository.VinRepository;
import com.opencsv.CSVReader;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;


public class NMVTIS {
    @Autowired
    private final VinRepository repo;


    @Autowired
    public NMVTIS(VinRepository repo) {
        this.repo = repo;
    }

    public void run()
    {


        try (
                Reader reader = Files.newBufferedReader(Paths.get("C:/Users/Mark/Desktop/VIN_data2.csv"))
        ) {

                CSVReader csvReader = new CSVReader(reader);

                String[] nextRecord;
                int i = 0;
                while ((nextRecord = csvReader.readNext()) != null) {



                    VINEntry newEntry = new VINEntry();
                    String dateString = nextRecord[0];
                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy HH:mm a");
                    newEntry.setNMVTISDate(sdf.parse(dateString));
                    newEntry.setVin(nextRecord[1]);
                    newEntry.setVinNumber((Integer.parseInt(nextRecord[2])));
                    newEntry.setInNMVTIS(true);
                    newEntry.setFound(false);
                    System.out.println("VIN : " + newEntry.getNMVTISDate());
                    System.out.println("VIN : " + newEntry.getVin());
                    System.out.println("VIN : " + newEntry.getVinNumber());
                    System.out.println("---------------------------" + i);


                i++;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
