package DataScraper.CSV;

import DataScraper.Model.VINEntry;
import DataScraper.Service.VinService;
import DataScraper.Utils.Duplicates;
import com.opencsv.CSVReader;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.List;

public class EUCSV {
    @Autowired
    private final VinService vinService;


    @Autowired
    public EUCSV(VinService vinService) {
        this.vinService = vinService;
    }

    public void run()
    {

        try (
                Reader reader = Files.newBufferedReader(Paths.get("C:/Users/Mark/Desktop/Java Projects/DataScraper/EU.csv"))
        ) {
                CSVReader csvReader = new CSVReader(reader);

                String[] nextRecord;
                int i = 0;
                while ((nextRecord = csvReader.readNext()) != null) {
                    boolean A = false;
                    if(nextRecord[0].contains("A")){
                        A = true;
                    }
                    String VIN = "";
                    List<VINEntry> vins = vinService.findByVinNumber(Integer.parseInt(nextRecord[1]));
                    for(VINEntry vin : vins){
                        if(vin.getVin().contains("A") && A){
                            VIN = vin.getVin();
                        }else if(vin.getVin().contains("B") && !A){
                            VIN = vin.getVin();
                        }
                    }
                    System.out.println("Matched VIN is: " + VIN);
                    List<VINEntry> vin = vinService.findByVin(VIN);
                    if(vin.size() == 1){
                        VINEntry tempVin = vin.get(0);
                        tempVin.setNonUS(true);
                        tempVin.setFound(true);
                        System.out.println("VIN : " + tempVin.getVin());
                        System.out.println("VIN : " + tempVin.getVinNumber());
                        System.out.println("---------------------------" + i);
                        vinService.save(tempVin);
                        Duplicates.test(vinService, tempVin);
                    }

                i++;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
