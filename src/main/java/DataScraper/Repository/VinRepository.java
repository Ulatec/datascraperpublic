package DataScraper.Repository;

import DataScraper.Model.VINEntry;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;


@RepositoryRestResource
public interface VinRepository extends PagingAndSortingRepository<VINEntry, String> {

    List<VINEntry> findByVin(String VIN);
    List<VINEntry> findByFoundFalse();
    List<VINEntry> findByFoundTrue();
    List<VINEntry> findByFoundFalseAndInNmvtisFalseAndCarfaxFoundFalseAndAutoCheckFoundFalseAndVinNumberGreaterThan(int i);
    List<VINEntry> findByFoundFalseAndInNmvtisFalseAndCarfaxFoundFalseAndNonUSFalse();
    List<VINEntry> findByInNmvtisFalse();
    List<VINEntry> findByCarfaxFoundFalse();
    List<VINEntry> findByCarfaxFoundTrueAndInNmvtisFalseAndStateIsNull();
    List<VINEntry> findByVinNumber(int vinNumber);
    List<VINEntry> findByInNmvtisFalseAndNonUSFalse();
    List<VINEntry> findByInNmvtisTrueOrCarfaxFoundTrueOrFoundTrue();
    List<VINEntry> findByInNmvtisTrue();
    List<VINEntry> findByIdGreaterThanAndIdLessThan(Long i, Long j);
    List<VINEntry> findByVinNumberGreaterThanEqualAndVinNumberLessThanEqual(int i, int j);
    List<VINEntry> findByCarfaxFoundFalseAndNonUSFalse();
    List<VINEntry> findByAutoCheckFoundFalseAndNonUSFalse();
    List<VINEntry> findByAutoCheckFoundTrueAndInNmvtisFalseAndStateIsNull();
    List<VINEntry> findByAutoCheckFoundTrue();
}
