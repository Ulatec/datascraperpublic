Spring Boot application for scraping and maintaining data Tesla Model 3 information.

Data is scraped from multiple government or third party sites.

Data maintained in a remote H2 database, but could be done with an in-memory database.

